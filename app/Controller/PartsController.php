<?php
App::uses('Controller', 'AppController');
App::import('Lib','Formfilter');
/**
 * parts controller
 * 
 * @author thinking
 * @property Part $Part
 */
class PartsController extends AppController {
	public $uses = array('Part');

	public $paginate = array(
		'joins'=>array(
	        array(
				'alias' => 'PartCategory',
				'table' => 'part_category',
		        'type' => 'LEFT',
		        'conditions' => 'Part.id = PartCategory.part_id',
	        )
	    )
	);

/**
 * beforeFilter
 *
 * @return void
 */
	public function beforeFilter() {		
		parent::beforeFilter();
		$url = $this->request->here;
		if( strrpos($url, '.jpg') > 0){
			var_dump($url);die();
		}
		if( strrpos($url, '.gif') > 0){
			var_dump($url);die();
		}
		if( strrpos($url, '.png') > 0){
			var_dump($url);die();
		}
	}
	
	public function index(){
		$categories = $this->Part->Category->find('all', array(
			'conditions'=>array(
				'Category.parent_id'=>4161
			)
		));

		$this->set('categories', $categories);
		$this->set('title_for_layout',  'ICkey 芯片代购' );
	}

	public function category($category_id = false){
		//if Could not find that category, try first category of 4165.
		$category_id_firstshow = 4165;
		if(!$category_id){
			//throw new NotFoundException('Could not find that category');
			$category_id = $category_id_firstshow;
		}
		$category = $this->Part->Category->read(null, $category_id);
		if(!$category){
			$this->Session->setFlash("category id {$category_id} not exist");			
			$category_id = $category_id_firstshow;
			return $this->category($category_id_firstshow);
		}

		$categories = Cache::read('xoct_cates' . $category_id, 'longterm');
		if (!$categories) {
		    $categories = $this->Part->Category->find('all', array(
				'conditions'=>array(
					'Category.parent_id' => $category_id
				)
			));
		    Cache::write('xoct_cates' . $category_id, $categories, 'longterm');
		}

		// apply filters if session data exist or just paginate 
		$this->_check_filters();
		$sessValue = $this->Session->read(SESSION_FILTER_KEY);

		if($sessValue && !property_exists($sessValue,'categoryId')){
			$sessValue = false;			
		}
		if($sessValue && property_exists($sessValue,'categoryId') &&$sessValue->categoryId == $category_id) {			
			$ids = $this->_getByFilters($category_id, $sessValue);
			$conditions = array(
				'PartCategory.category_id' => $category_id,
				'Part.id' => $ids,
			);
			$this->set('sessionFilterCondition' , $sessValue);
			$parts = $this->paginate($conditions);
		}else{			
			$this->set('sessionFilterCondition' , false);
			$conditions = array(
				'PartCategory.category_id' => $category_id,
			);
			$parts = $this->paginate($conditions);
		}

		$part_attributes = Cache::read('xoct_attrs' , 'longterm');
		if (!$part_attributes) {
			$part_attributes = $this->Part->Spec->PartAttribute->find('all', array(
				'fields'=>array('PartAttribute.*'),
				'conditions'=>$conditions,
				'joins'=>array(
					array(
						'alias' => 'Specs',
						'table' => 'specs',
				        'type' => 'LEFT',
				        'conditions' => 'PartAttribute.id = Specs.part_attribute_id',
					),
					array(
						'alias' => 'PartCategory',
						'table' => 'part_category',
				        'type' => 'LEFT',
				        'conditions' => 'Specs.part_id = PartCategory.part_id',
			        )
				),
				'group'=>'PartAttribute.id'
			));
			Cache::write('xoct_attrs' , $part_attributes, 'longterm');
		}

		foreach($part_attributes as &$part_attribute){
			if($part_attribute['PartAttribute']['type']=='text'){
				$values = $this->Part->Spec->find('all', array(
					'fields'=>array('Spec.value'),
					'conditions'=>array('Spec.part_attribute_id'=>$part_attribute['PartAttribute']['id']),
					'joins'=>array(
						array(
							'alias' => 'PartCategory',
							'table' => 'part_category',
					        'type' => 'LEFT',
					        'conditions' => 'Spec.part_id = PartCategory.part_id',
				        )
					),
					'group'=>'Spec.value'
				));
				$values = Set::extract('{n}.Spec.value', $values);

				$part_attribute['values'] = $values;
			}else{
				$max = $this->Part->Spec->find('first', array(
					'fields'=>array('Spec.floatvalue'),
					'conditions'=>array('Spec.part_attribute_id'=>$part_attribute['PartAttribute']['id']),
					'joins'=>array(
						array(
							'alias' => 'PartCategory',
							'table' => 'part_category',
					        'type' => 'LEFT',
					        'conditions' => 'Spec.part_id = PartCategory.part_id',
				        )
					),
					'order'=>'Spec.floatvalue DESC'
				));

				$max = $max['Spec']['floatvalue'];
				$part_attribute['max'] = $max;

				$min = $this->Part->Spec->find('first', array(
					'fields'=>array('Spec.floatvalue'),
					'conditions'=>array('Spec.part_attribute_id'=>$part_attribute['PartAttribute']['id']),
					'joins'=>array(
						array(
							'alias' => 'PartCategory',
							'table' => 'part_category',
					        'type' => 'LEFT',
					        'conditions' => 'Spec.part_id = PartCategory.part_id',
				        )
					),
					'order'=>'Spec.floatvalue'
				));

				$min = $min['Spec']['floatvalue'];
				$part_attribute['min'] = $min;
			}
		}

		$this->Part->contain(array(
			'Brand',
		));

		$manufacturers = Cache::read('xoct_manufacts' . $category_id , 'longterm');
		if (!$manufacturers) {
			$manufacturers = $this->Part->find('all', array(
				'fields'=>array('Brand.*'),
				'conditions'=>array(
					'PartCategory.category_id' => $category_id,
				),
				'joins'=>array(
			        array(
						'alias' => 'PartCategory',
						'table' => 'part_category',
				        'type' => 'LEFT',
				        'conditions' => 'Part.id = PartCategory.part_id',
			        )
			    ),
			    'group'=>'Brand.id',
			));
			Cache::write('xoct_manufacts' . $category_id , $manufacturers, 'longterm');
		}

		$this->Part->contain();

		$suppliers = Cache::read('xoct_suppliers' . $category_id , 'longterm');
		if (!$suppliers) {
			$suppliers = $this->Part->Brand->find('all', array(
				'fields'=>array('Brand.*'),
				'conditions'=>array(
					'PartCategory.category_id' => $category_id,
				),
				'joins'=>array(
					array(
						'alias' => 'Offer',
						'table' => 'offers',
				        'type' => 'LEFT',
				        'conditions' => 'Brand.id = Offer.brand_id',
					),
			        array(
						'alias' => 'PartCategory',
						'table' => 'part_category',
				        'type' => 'LEFT',
				        'conditions' => 'Offer.part_id = PartCategory.part_id',
			        )
			    ),
			    'group'=>'Brand.id',
			    'recursive'=>-1
			));
			Cache::write('xoct_suppliers' . $category_id , $manufacturers, 'longterm');
		}
		$this->set('category', $category);
		$this->set('categories', $categories);
		$this->set('parts', $parts);
		$this->set('part_attributes', $part_attributes);
		$this->set('manufacturers', $manufacturers);
		$this->set('suppliers', $suppliers);
		
		if('ickey' != $this->octTheme ) {
			$this->render('category-default-theme');
		}else{
			$this->render('category', $this->octTheme);				
		}
		$this->set('title_for_layout',  'ICkey代购产品型号列表' );
	}

	public function view($id){
		if(!$id){
			throw new NotFoundException('Could not find that part');
		}

		$this->Part->contain(array(
			'Brand',
			'Category',
			'Datasheet',
			'Description',
			'Image',
			'Offer'=>array('Price', 'Brand'),
			'Spec'=>array('PartAttribute')
		));

		$part = $this->Part->read(null, $id);

		$categories = $this->Part->Category->find('all', array(
			'conditions'=>array(
				'Category.parent_id'=>4161
			)
		));

		$authorized_seller_numbers = array();
		$non_authorized_seller_numbers = array();
		$non_authorized_dealers_numbers = array();

		foreach($part['Offer'] as &$offer){
			$prices = array();
			foreach($offer['Price'] as $price){
				if($offer['is_authorized']){
					$authorized_seller_numbers[$price['number']] = true;
				}elseif($offer['is_brokered']){
					$non_authorized_seller_numbers[$price['number']] = true;
				}else{
					$non_authorized_dealers_numbers[$price['number']] = true;
				}

				$prices[$price['number']] = $price['price'];
			}
			$offer['prices'] = $prices;
		}

		$authorized_seller_numbers = array_keys($authorized_seller_numbers);
		sort($authorized_seller_numbers);
		$non_authorized_seller_numbers = array_keys($non_authorized_seller_numbers);
		sort($non_authorized_seller_numbers);
		$non_authorized_dealers_numbers = array_keys($non_authorized_dealers_numbers);
		sort($non_authorized_dealers_numbers);

		//第一个所属类别，以便在基于 keyword 的search中可以继续 浏览
		if(count($part['Category']) < 2){
			$this->set('formCategoryId', 4166); //root 被动元器件 电容
		}else{
			if( $part['Category'][0]['id'] != 4161){
				$this->set('formCategoryId', $part['Category'][0]['id']);
			}else{
				$this->set('formCategoryId', $part['Category'][1]['id']);
			}
		}
		
		$this->set('part', $part);
		$this->set('categories', $categories);
		$this->set('authorized_seller_numbers', $authorized_seller_numbers);
		$this->set('non_authorized_seller_numbers', $non_authorized_seller_numbers);
		$this->set('non_authorized_dealers_numbers', $non_authorized_dealers_numbers);		
		$this->set('total_seller' , count($authorized_seller_numbers) + count($non_authorized_seller_numbers) + count($non_authorized_dealers_numbers) );
		$this->set('title_for_layout',  'IC代购产品型号' . $part['Part']['mpn'] . '芯片代购' );
		
	}


	private function _getByFilters($category_id, $formFilter){
		$category_id = intval($category_id);
		
		$ids_C = array(); //Category id
		$ids_M = array();//Manufactors
		$ids_S = array(); //Suppliers
		$ids_K = array(); //Keywords
		$ids_A = array(); //Attrib filter

		// 纯粹依靠 category_id 的过滤条件
		$ids_Result = $ids_C = $this->_getIdsByCategoryId($category_id);

		//厂牌
		if(isset($formFilter->q_manufacturers) && strlen($formFilter->q_manufacturers) > 4) {
			$ids_M = $this->_getIdsByManufactors($formFilter->q_manufacturers);
			$ids_Result = int_array_intersect($ids_M, $ids_C);			
		}

		//供应商
		if(isset($formFilter->q_suppliers) && strlen($formFilter->q_suppliers) > 4) {
			$ids_S = $this->_getIdsBySuppliers($formFilter->q_suppliers);
			$ids_Result = int_array_intersect($ids_Result, $ids_S);			
		}
		
		//关键词
		if(isset($formFilter->q_keyword) && strlen($formFilter->q_keyword) > 1) {
			$ids_K = $this->_getIdsByKeyword($formFilter->q_keyword);
			$ids_Result = int_array_intersect($ids_Result, $ids_K);
		}
		
		$have_attr_filter = false;
		//如果存在属性过滤条件		
		if(isset($formFilter->attribs) && count($formFilter->attribs) > 0) {
			$have_attr_filter = true;
			$wheresql = 'SELECT `Spec`.`part_id` FROM specs AS  `Spec` WHERE 1=1 ';
			
			foreach($formFilter->attribs as $key => $item){
				$where_f = '';
				{
					$where_f =   'AND `Spec`.`part_attribute_id` = ' .  intval($key)  .  '  AND  '  ;				
			
					if( $item['q_is_text'] ){
						$queryTextArr = explode(',' , $item['q_text']);
						foreach($queryTextArr as $index => $value){
							if( empty($value)){
								unset($queryTextArr[$index]);
							}
						}
						if( count($queryTextArr) > 0){
							$where_f .= '( '  ;
							foreach($queryTextArr as $index => $value){
								$where_f = $where_f . " `Spec`.`value` = '"  . mysql_escape_string($value) . "'  "  ;
								if($index != count($queryTextArr) -1 ){
									$where_f = $where_f .   ' OR '  ;
								}
							}
							$where_f .= ') ' ;
						}
					}else{
						//数值型单位，可能涉及到毫 微 纳 皮
						$queryUnit =  intval($item['q_unit']);
						$queryUnitMulti = 1;
						if(4 == $queryUnit ){
							$queryUnitMulti = 10E3;
						}else if(8 == $queryUnit ){
							$queryUnitMulti = 10E6;
						}else if(16 == $queryUnit ){
							$queryUnitMulti = 10E9;
						}else if(32 == $queryUnit ){
							$queryUnitMulti = 10E12;
						}else{
							$queryUnitMulti = 1;
						}
						if( $item['q_is_exact'] ){
							$queryExact = floatval($item['q_exact_value']) ;
							$where_f = $where_f .  "ABS( {$queryUnitMulti} * `Spec`.`floatvalue` -  $queryExact) < 13  "   ;
						}else{
							$queryMin = floatval($item['q_min_value'])  ;
							$queryMax = floatval($item['q_max_value']) ;
							$where_f = $where_f .  "( {$queryUnitMulti} * `Spec`.`floatvalue`  <= {$queryMax}  AND  {$queryUnitMulti} * `Spec`.`floatvalue`  >= {$queryMin} )"   ;
						}
					}
				}
				$wheresql .= $where_f;
			}
			//出于性能考虑，最多50万条
			$wheresql = $wheresql . ' LIMIT 500000 ';			
		}		
		if($have_attr_filter){		
			$ids_A = $this->_getIdsByAttrs($category_id,$wheresql);
			$ids_Result = int_array_intersect($ids_Result, $ids_A);
		}
		
		return $ids_Result;
	}


	//属性过滤条件，查出所有的 part_id
	private function _getIdsByAttrs($category_id ,$wheresql){
		//先查cache中有无数据  
		$cache_key  = $category_id . '_' .  sha1( $wheresql ) . '-' . md5( $wheresql );
		$cache_data = Cache::read( $cache_key, 'partids');
		if($cache_data){			
			return $cache_data;
		}else{
			//无cache，查询数据库
			$ids = $this->Part->query($wheresql);
			$id_liststr = '';
			if($ids && count($ids) > 1){
				foreach($ids as $item){
					$id_liststr .= $item['Spec']['part_id'] . ',' ;
				}
				$id_liststr .=  '-1';
			}else{
				return null;
			}
		
			$wheresql = 'SELECT part_id FROM `part_category` AS P WHERE category_id='.  $category_id  . ' AND part_id IN  ('  . $id_liststr  . ' )';
			
			$ids = $this->Part->query($wheresql);
			$id_list = array();
			if($ids && count($ids) > 1){
				foreach($ids as $item){
					$id_list[] = $item['P']['part_id'];
				}
			}
			Cache::write( $cache_key, $id_list, 'partids');			
			return $id_list;
		}
	}

	//CategoryId过滤条件，查出所有的 part_id
	private function _getIdsByCategoryId($category_id){
		//先查cache中有无数据  
		$cache_key  = $category_id . '_bycateid_' . $category_id ;
		$cache_data = Cache::read( $cache_key, 'partids');
		if($cache_data){
			return $cache_data;
		}
		//无cache，查询数据库
		$wheresql = 'SELECT part_id FROM `part_category` AS P WHERE P.category_id='.  $category_id ;			
		$ids = $this->Part->query($wheresql);
		$id_list = array();
		if($ids && count($ids) > 1){
			foreach($ids as $item){
				$id_list[] = $item['P']['part_id'];
			}
		}
		Cache::write( $cache_key, $id_list, 'partids');			
		return $id_list;
	}

	//厂牌过滤条件，查出所有的 part_id 参见 parts表 brand_id 字段
	private function _getIdsByManufactors( $manufactor_string){
		//先查cache中有无数据  
		$cache_key  =   'byManufactors_' . md5($manufactor_string);
		$cache_data = Cache::read( $cache_key, 'partids');
		if($cache_data){
			return $cache_data;
		}
		//无cache，查询数据库
		$array_brand_id = explode(',', $manufactor_string . '-1'  );
		$id_list = array();
		if( count($array_brand_id) > 1){
			foreach($array_brand_id as $key => $item){
				if($item < 0){
					unset($array_brand_id[$key]);
				}				
			}
			$wheresql = implode(',', $array_brand_id);
			$wheresql = 'SELECT id FROM `parts` AS P WHERE P.brand_id IN('.  $wheresql . ')' ;			
			$ids = $this->Part->query($wheresql);
			if($ids && count($ids) > 1){
				foreach($ids as $item){
					$id_list[] = $item['P']['id'];
				}
			}
			Cache::write( $cache_key, $id_list, 'partids');		
		}
		return $id_list;
	}
	
	//供应商过滤条件，查出所有的 part_id  参见 offers表 brand_id 字段
	private function _getIdsBySuppliers( $supplier_string){
		//先查cache中有无数据  
		$cache_key  =   'bySuppliers_' . md5($supplier_string);
		$cache_data = Cache::read( $cache_key, 'partids');
		if($cache_data){
			return $cache_data;
		}
		//无cache，查询数据库
		$array_brand_id = explode(',', $supplier_string . '-1'  );
		$id_list = array();
		if( count($array_brand_id) > 1){
			foreach($array_brand_id as $key => $item){
				if($item < 0){
					unset($array_brand_id[$key]);
				}				
			}
			$wheresql = implode(',', $array_brand_id);
			$wheresql = 'SELECT part_id FROM `offers` AS P WHERE P.brand_id IN('.  $wheresql . ')' ;			
			$ids = $this->Part->query($wheresql);
			if($ids && count($ids) > 1){
				foreach($ids as $item){
					$id_list[] = $item['P']['part_id'];
				}
			}
			Cache::write( $cache_key, $id_list, 'partids');		
		}
		return $id_list;
	}

	//关键词过滤条件，查出所有的 part_id  参见 parts 表 mpn 字段 可使用 sphinx 引擎全文检索
	private function _getIdsByKeyword( $keyword_string){
		//先查cache中有无数据  
		$cache_key  =   'byKeywords_' . md5($keyword_string);
		$cache_data = Cache::read( $cache_key, 'partids');
		if($cache_data){
			return $cache_data;
		}
		//无cache，查询数据库			
		$id_list = array();
		$wheresql = 'SELECT id FROM `parts` AS P WHERE P.mpn LIKE '  . "  '%{$keyword_string}%'  "  ;
		$ids = $this->Part->query($wheresql);		
		if($ids && count($ids) > 1){
			foreach($ids as $item){
				$id_list[] = $item['P']['id'];
			}
		}
		Cache::write( $cache_key, $id_list, 'partids');		
		
		return $id_list;
	}
	
	private function _getByIds($id_list){
		if( count($id_list) > 50){
			$id_list = array_splice($id_list, 0, 10);
		}
		$conditions['Part.id'] = $id_list;	
		$data = $this->Part->find('all', array(			
			'conditions' => $conditions
		));		
		return $data;
	}
	
	
	private function _check_filters(){		
		$sessValue = $this->Session->read(SESSION_FILTER_KEY);	
		
		if($sessValue &&  property_exists($sessValue,'attribs')  ){			
			if( count($sessValue->attribs) < 1){
				unset($sessValue->attribs);
				$this->Session->write(SESSION_FILTER_KEY, $sessValue);
			}
		}
	}
}
