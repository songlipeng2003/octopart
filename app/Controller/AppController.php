<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright	 Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link		  http://cakephp.org CakePHP(tm) Project
 * @package	   app.Controller
 * @since		 CakePHP(tm) v 0.2.9
 * @license	   MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');
App::import('Sanitize');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package	   app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
		public $components = array('Session', 'Cookie', 'RequestHandler');
		
/**
 * Helpers
 *
 * @var array
 * @access public
 */
    public $helpers = array(
        'Html',
        'Form',
        'Session'       
    );
/**
 * beforeFilter
 *
 * @return void
 */
	public function beforeFilter() {
		$this->Cookie->name = 'CMgr';
		$octTheme = false;
		if ( $this->Cookie->read('octTheme') ){
			$octTheme = trim( $this->Cookie->read('octTheme') ) ;
		}
		if ( isset($this->params->query['octTheme']) ){
			$octTheme = trim( $this->params->query['octTheme'] );			
		}
		//set default layout theme is ickey
		if($octTheme == false || $octTheme == ''){
			$octTheme = 'ickey';
		}
		if($octTheme != false){			
			$this->layout = $octTheme;
			$this->Cookie->write('octTheme', $octTheme);
		}
		$this->octTheme = $octTheme;
		$this->set('octTheme', $octTheme);
	}
}
