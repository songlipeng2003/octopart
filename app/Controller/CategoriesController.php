<?php
App::uses('Controller', 'AppController');

/**
 * categories controller
 * 
 * @author thinking
 * @property Category $Category
 */
class CategoriesController extends AppController {
	var $uses = array('Category');
	public function index($category_id=null){
		$category_id = $category_id==null?4161:$category_id;
		$category = $this->Category->read(null, $category_id);
		if(!$category){
			throw new NotFoundException('Could not find that category');
		}
		$this->Category->recursive = 1;
		$categories = $this->Category->find('all', array(
			'conditions'=>array(
				'Category.parent_id'=>$category_id
			)
		));
		
		foreach($categories as &$cate){
			$cate['Category']['num_children'] = count($cate['ChildCategory']);
		}
		
		$categories = Set::sort($categories, '{n}.Category.num_children', 'desc');
		
		$this->set('category', $category);
		$this->set('categories', $categories);
	}
}
