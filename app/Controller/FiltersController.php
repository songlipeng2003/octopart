<?php
App::uses('Controller', 'AppController');
App::import('Lib','Formfilter');
/**
 * Filters controller
 * 
 * @author zhouyong
 * @property Part $Part
 */
class FiltersController extends AppController {
	public $uses = array('Part');

	public function index(){
		$this->redirect(array('plugin'=>null,'controller'=>'parts','action' => 'index'));
	}

/**
 * 设置过滤条件
 *
 */
	public function set_filter(){
		//$this->layout = 'ajax';
		$result = array('resullt' => false);
		if(!property_exists($this,'data')  || empty($this->data)  ){
			if($this->request && $this->request['data'] && $this->request['data']['RequestFilter']){
				$this->data = array('RequestFilter' => $this->request['data']['RequestFilter'] );
			}else{
				$this->redirect(array('plugin'=>null,'controller'=>'parts','action' => 'category'));		
				return;
			}
		}
		
		$requestFilter = new FormFilter();		
		$data = $this->data['RequestFilter'];
		
		$requestFilter->categoryId = intval($data['q_categoryid'] );		
		$requestFilter->attribs = array();		
		$requestFilter->q_attributeid = $q_attributeid = intval($data['q_attributeid'] );
		$requestFilter->q_manufacturers = $q_manufacturers = $data['q_manufacturers'] ;
		$requestFilter->q_suppliers = $q_suppliers = $data['q_suppliers'] ;
		
		$q_keyword = trim($data['q_keyword']);
		//关键字查询最长18字符
		if( strlen($q_keyword) > 18){
			$q_keyword = substr($q_keyword, 0, 18);
		}
		$requestFilter->q_keyword = $q_keyword ;
		
		//Invalid request
		if($requestFilter->categoryId < 4000 || $requestFilter->categoryId > 5999){		
			var_dump( $requestFilter);die();				
			$this->Session->setFlash('Invalid request:bad categoryId ');
			$this->Session->write(SESSION_FILTER_KEY, false);		
			$this->redirect(array('plugin'=>null,'controller'=>'parts','action' => 'category', 0));
			return;
		}

		//原来session中的值，尽量保留
		$sessValue = $this->Session->read(SESSION_FILTER_KEY);		
		//若不存在，或不是相应的categoryId，则没有必要保留
		if(!$sessValue){
			$sessValue = $requestFilter;
		}else{			
			if(!property_exists($sessValue,'categoryId')){
				$sessValue = $requestFilter;	
			}
		}

		if ($sessValue->categoryId != $requestFilter->categoryId){			
			$sessValue = $requestFilter;			
		}
		
//如果request中有新值，则替换session
		if( '' == $q_keyword){
			
		}else{
			$sessValue->q_keyword = mysql_escape_string($q_keyword );
		}
		
		if( '0' != $q_manufacturers){						
			$sessValue->q_manufacturers  = mysql_escape_string($requestFilter->q_manufacturers );
		}
		
		if( '0' != $q_suppliers){			
			$sessValue->q_suppliers  = mysql_escape_string( $requestFilter->q_suppliers );
		}

//属性筛选 
		if( $q_attributeid > 0){		
			$temp  = $this->data['RequestFilter'];
			if(isset($temp['q_manufacturers'])) unset($temp['q_manufacturers']);
			if(isset($temp['q_suppliers'])) unset($temp['q_suppliers']);
			if(isset($temp['q_keyword'])) unset($temp['q_keyword']);			
			$sessValue->attribs[$q_attributeid] = $temp;
		}		
		unset($sessValue->q_attributeid);
		unset($sessValue->data); 
		$this->Session->write(SESSION_FILTER_KEY, $sessValue);		
		
		$result['resullt'] = true;
		$this->set('result', $result);

		$this->redirect(array('plugin'=>null,'controller'=>'parts','action' => 'category', $requestFilter->categoryId));
		//$this->redirect(array('plugin'=>null,'controller'=>'filters','action' => 'get_filter'));
	}

	/**
	 * 查看当前的 过滤器
	 * 
	 */
	public function get_filter(){		
		$this->set('result',   $this->Session->read(SESSION_FILTER_KEY));
	}

	/**
	 * 移除一个属性过滤项
	 *
	 */
	public function remove_one_filter($attribute_id){
		$result = array('resullt' => false);
		$sessValue = (object) $this->Session->read(SESSION_FILTER_KEY);
		if(!$sessValue){
			$this->set('result',   $result);
			$this->Session->write(SESSION_FILTER_KEY, false);
			return;
		}
		
		if (isset($sessValue->attribs[$attribute_id]) ){
			unset($sessValue->attribs[$attribute_id] );
		}
		$this->Session->write(SESSION_FILTER_KEY, $sessValue);
		$result['resullt'] = true;
		$this->set('result',   $result);
		$this->_check_filters();
	}

	/**
	 * 移除一个过滤项（厂商、供应商、关键词）
	 *
	 */
	public function remove_other_filter($whatfilter = ''){
		$result = array('resullt' => false);
		$whatfilter = trim( strtolower($whatfilter) );
		$allowed = array('q_manufacturers', 'q_suppliers', 'q_keyword');
		if(in_array($whatfilter, $allowed)){
			$sessValue = $this->Session->read(SESSION_FILTER_KEY);
			if('q_manufacturers' == $whatfilter ){
				unset( $sessValue->q_manufacturers );
			}else if('q_suppliers' == $whatfilter ){
				unset( $sessValue->q_suppliers );
			}else if('q_keyword' == $whatfilter ){
				unset( $sessValue->q_keyword );
			}
			
			$this->Session->write(SESSION_FILTER_KEY, $sessValue);
			$result['resullt'] = true;
			$this->set('result',   $result);
			return;
		}
		$this->set('result',   $result);
		$this->_check_filters();
	}
	
	/**
	 * 移除所有过滤项
	 *
	 */
	public function remove_all_filter(){
		$result = array('resullt' => true);
		$this->Session->write(SESSION_FILTER_KEY, false);
		$this->set('result',  $result);
	}

	/**
	 * 
	 *
	 */
	private function _check_filters(){		
		$sessValue = $this->Session->read(SESSION_FILTER_KEY);
		
		if($sessValue &&  property_exists($sessValue,'attribs')  ){
			if( count($sessValue->attribs) < 1){
				unset($sessValue->attribs);
				$this->Session->write(SESSION_FILTER_KEY, $sessValue);
			}
		}
	}
}
