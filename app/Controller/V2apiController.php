<?php
App::uses('Controller', 'AppController');
App::import('Lib', 'Web');
/**
 * api version 2 controller
 * 
 * @author thinking
 * @property Part $Part
 */
class V2apiController extends AppController {
	public $uses = array('Part');
	
	private $session_filter_key = 'SessionFilterCondition';
	
	public $paginate = array(
		'joins'=>array(
	        array(
				'alias' => 'PartCategory',
				'table' => 'part_category',
		        'type' => 'LEFT',
		        'conditions' => 'Part.id = PartCategory.part_id',
	        )
	    )
	);
	
/**
 * 若参数中含有callback 或 jsonp 支持回调函数
 * 
 */
	private function _processCallback(){
		if( isset($this->request->query['callback']) ){
			$callback = trim($this->request->query['callback']);
			$this->set('callback', $callback);
			return;
		}else{
			$this->set('callback', false);
		}
		//support jsonp
		if( isset($this->request->query['jsonp']) ){
			$callback = trim($this->request->query['jsonp']);
			$this->set('callback', $jsonp);
		}
	}
	
	public function index(){
		$result = array();
		$this->set('result', $result);
	}
	
	public function get_multi(){
		$this->_processCallback();
		$uids = false;
		if( isset($this->request->query['uids']) ){
			$uids = trim($this->request->query['uids']);
			$pos1 = strpos($uids, '[');
			$pos2 = strpos($uids, ']');			
			if( $pos2 > $pos1){
				$uids = substr($uids, $pos1 + 1, ($pos2 - $pos1 - 1) );
				$uids = array_map('intval', explode(',', $uids));			
			}
		}
		//var_dump($uids);				
		if( is_array($uids) ){
			$parts = $this->Part->find('all', array(
				'conditions' => array(
					'Part.id' => $uids
				),
				'limits' => 100
			));			
		}else{
			$parts = array();
		}
		//var_dump($parts);
		$this->set('result', $parts);
	}
	
	//通过octo API 接口取数据 
	public function remote_get_multi(){
		$proxy = array(
			'host'=>'127.0.0.1',
			'port'=>'7071',
			'type'=>'socks5'
		);
		
		$pre_url = array(
			'http://octopart.com/api/v2/parts/get_multi?apikey=e795a10c&uids=',
			'http://octopart.com/api/v2/parts/get_multi?apikey=e795a10c&uids=', //more api key
		);
		
		$this->_processCallback();
		
		$uids = false;		
		if( isset($this->request->query['uids']) ){
			$uids = trim($this->request->query['uids']);
			
			$uids = urlencode($uids );
			
			$url = $pre_url[0] . $uids;
		}else{
			$this->set('result', false);
			return;
		}

		$parts = $content = false;
		
		if( $uids) {			
			$content = web_fetch($url, array(
				'is_retry'=>true,
				'try_times'=>3,
				'agent' => 'firefox', 
				'cookie' => true,
				
			));
			$test = false;
			if( strlen($content ) > 200){				
				@ $test = json_decode($content);
			}
			if($test){
				$this->set('result', $content);
				return;
			}			
			$content = web_fetch($url, array(
				'is_retry'=>true,
				'try_times'=>3,		
				'agent' => 'firefox', 
				'cookie' => true,	
				'proxy'	=> $proxy
			));
			$test = false;
			if( strlen($content ) > 200){				
				@ $test = json_decode($content);
			}
			if($test){
				$this->set('result', $content);
				return;
			}else{
				$parts = false;
			}
		}else{
			$parts = false;
		}
		$this->set('result', $parts);
	}
}
