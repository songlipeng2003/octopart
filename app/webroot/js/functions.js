﻿/** utf-8 encoding file js. ik is short for ic key */
ik={};
ik.data = {};
ik.data2 = {};
ik.maxArrtibFilter = 5;
ik.Api = {};
ik.Api.Host = 'SameButRemote';
//ik.Api.Host = 'SameDomain';
ik.log = function(text){
	if($.browser.mozilla && typeof(console) == 'object') { 
		console.log(text);
	}
}

ik.processData=function(data){
	if(ik.Api.Host == 'OrgiDomain'){
		ik.processDataOrgiDomain(data);
		return;
	}else if(ik.Api.Host == 'SameButRemote'){
		ik.processDataSameButRemote(data);
		return;
	}else if(ik.Api.Host == 'SameDomain'){
		ik.processDataSameDomain(data);
		return;
	}
}

ik.processDataOrgiDomain=function(){
}

ik.processDataSameDomain=function(){	
}

ik.processDataSameButRemote=function(data){	
	for(var index in data){	
		//页面上的表格对象
		var table_obj = $('#oPart_' + data[index].uid);
		if( !table_obj){
			continue;
		}
		var offers = data[index].offers;			
		table_obj.find('.preData').remove();
		table_obj = table_obj.find('tbody');
		var avail_total = 0;
		for(var i in offers){			
			if(offers[i].avail > 0){				
				avail_total ++;
				var prices = offers[i].prices;
				var string = '<tr><td><a href="' + offers[i].supplier.homepage_url +'" target="_blank" >' + offers[i].supplier.displayname + '</a></td>' ;
				string += '<td>' +  offers[i].sku + '</td><td>' + offers[i].avail + '</td><td>';
				if(prices){					
					//起订量				
					for(var j in prices){						
						if(prices[ j]['0'] > 1){
							string +=  prices[ j]['0'] + '+<br/>';
						}else{
							string +=   prices[ j]['0'] + '<br/>';
						}
					}
					string += '</td><td>'
					//价格
					for(var j in prices){
						string +=  '$' + prices[ j]['1'] + '<br/>';
					}
					string += '<td>代购</td>';
				}else{
					string += '</td><td>-</td>-<td></td>';
					string += '<td></td>';
				}
				
				string += '</tr>';
				
				$(string).appendTo( table_obj );
			}
		}
	}	
}

/** get json data  */
ik.reqMultiIds=function(query_ids){		
	if(ik.Api.Host == 'OrgiDomain'){
		var url ='http://octopart.com/api/v2/parts/remote_get_multi?uids=' + query_ids + '&callback=ik.processData';
	}else if(ik.Api.Host == 'SameButRemote'){
		var url = '/api/v2/parts/remote_get_multi.json?uids=' + query_ids + '&callback=ik.processData';
	}else {
		var url = '/api/v2/parts/get_multi.json?uids=' + query_ids + '&callback=ik.processData';
	}
	$.getScript(url);
}

ik.cateInit=function(){	
	//属性过多，则只显示一部分，点seemore 才全出来
	setTimeout("$('.attr_filter .seemore').hide();", 200);

	//已应用了过滤条件 的属性、厂牌等，藏起来
	ik.initHideHasFilter();
	//当前的过滤条件
	ik.showCurrentFilter();

	//只允许单选 
	$('.siggroup').change(function(){
		if( 'checked' == $(this).attr('checked')){
			$('.siggroup').attr('checked', false) ;
			$(this).attr('checked', true) ;
		}else{
			$('.siggroup').attr('checked', false) ;
		}
	});
	
	//点击 seemore 看更多的属性项
	ik.attrSeeMore();
	//点击 应用筛选 时的事件
	ik.filterApply();
}

ik.submitFilter=function(){
	$('#PartFilterForm').submit();	
	$('#content .attr_filter').hide();
	$('.hua_search').hide();	
	$('.cake-sql-log').hide();	
	$('#product').hide();	
	$('#content').html('<br/><span style="font-size:18px;color:#0088CC;">Processing request ......<br/><img width="100" src="/images/loading2.gif" /></span>');
}

ik.initHideHasFilter=function(){
	$('.attr-filter-apply').each(function(i,element){
		var attrId = parseInt( $(element).data('attrid') );
		if( !isNaN(attrId) ){
			if( ik._findInCurrentByKey(attrId) ){
				var rightdiv = $(element).parent().parent();
				var welldiv = rightdiv.parent().parent();
				welldiv.find('.filterOptions').hide();
				rightdiv.html('已应用过滤');
			}
		}
	});
}

ik._findInCurrentByKey=function(attrId){
	if(typeof(currentFilter) != 'object'){
		return;
	}
	var str;
	var total = 0;
	for(var key in currentFilter){
		if( key == attrId) return true;
	}
	return false;
}

ik.showCurrentFilter=function(){
	if(typeof(currentFilter) != 'object'){
		return;
	}
	var total = 0;
	var str;
	for(var key in currentFilter){		
		if(key == 'attribs'  ){ 
			total ++;
			var attr_total = 0;
			var attribs = currentFilter.attribs ;
			ik.data = attribs;
			for(var attrib_id in attribs){
				attr_total ++;
				str = '<div class="item" ><a href="#1" class="removeOneFilter" data-attrid="';
				str = str +attribs[attrib_id].q_attributeid + '" >'  + '移除筛选条件</a><span class="desc">' +attribs[attrib_id].q_categoryname;
				if(attribs[attrib_id].q_is_text != '0'){
					str = str + '(文本匹配)';
					if(attribs[attrib_id].q_text.length > 20){
						str = str +attribs[attrib_id].q_text.substr(0,20) + '...';
					}else{
						str = str +attribs[attrib_id].q_text;
					}
				}else{
					str = str + '(数值匹配)';
					if(attribs[attrib_id].q_is_exact != '0' ){
						str = str + '取值约等于 ' + attribs[attrib_id].q_exact_value  + attribs[attrib_id].q_exact_unit  ;
					}else{
						str = str + '取值范围(' +attribs[attrib_id].q_min_value + ','  +attribs[attrib_id].q_max_value + ')';
					}
				}
				str = str +  '</span></div>';
				$(str).appendTo('#currentFilter');
				//隐藏筛选属性器 div 层
				$('#well_attrid_' + attrib_id).hide();
			}
			//已经应用了太多的属性过滤，不允许再增加更多，就隐藏“应用”按钮
			if(attr_total >= ik.maxArrtibFilter){
				$('div.filter_attributes div.pull-right label').hide();				
				$('div.filter_attributes div.pull-right').html('不可过滤筛选太多的项');
				$('div.filter_attributes div.filterOptions').hide();
			}
			
		}else if(key == 'q_manufacturers' && currentFilter.q_manufacturers!='0'){
			total ++;
			str = '<div class="item" ><a href="#1" class="removeOneFilter" data-filter="q_manufacturers"';
			str = str +' >'  + '移除筛选厂牌 </a><span class="desc">' + $.cookie('TextQManufacturers') ;
			//currentFilter.q_manufacturers;
			str = str +  '</span></div>';
			$('div.filter_manufacturers').hide();
			$(str).appendTo('#currentFilter');
		}else if(key == 'q_suppliers' && currentFilter.q_suppliers!='0' ){
			total ++;
			str = '<div class="item" ><a href="#1" class="removeOneFilter" data-filter="q_suppliers"';
			str = str +' >'  + '移除筛选供应商 </a><span class="desc">' + $.cookie('TextQSuppliers') ;
			//currentFilter.q_suppliers;
			str = str +  '</span></div>';
			$('div.filter_suppliers').hide();
			$(str).appendTo('#currentFilter');
		}else if(key == 'q_keyword' && currentFilter.q_keyword ){
			total ++;
			str = '<div class="item" ><a href="#1" class="removeOneFilter" data-filter="q_keyword"';
			str = str +' >'  + '移除筛选关键词 </a><span class="desc">' + currentFilter.q_keyword;
			str = str +  '</span></div>';
			$(str).appendTo('#currentFilter');
		}
	}
	str = '';
	if(total > 0){
		if(total > 1){
			str = '<div class="item" ><a href="#1" class="removeAllFilter" >移除所有过滤条件 </a></div> ';
			$(str).appendTo('#currentFilter');
		}
		$('#currentFilter').show();
	}

	//移除筛选条件
	$(document).on("click", "a.removeOneFilter", 
	function(){
		//厂牌 供应商 关键词 等
		var whatfilter = $(this).data('filter');
		if(typeof(whatfilter) == 'string' && whatfilter.length > 5){
			$.getJSON('/filters/remove_other_filter/' + whatfilter + '.json', function(data){
				$('#RequestFilterQAttributeid').val('-1');
				if('q_keyword' == whatfilter){
					$('#RequestFilterQKeyword').val('');
				}
				ik.submitFilter();
			});
			return;
		}
		//属性筛选
		var attrid = parseInt( $(this).data('attrid') );
		if( !isNaN(attrid) ){
			$.getJSON('/filters/remove_one_filter/' + attrid + '.json', function(data){
				$('#RequestFilterQAttributeid').val('-1');
				ik.submitFilter();
			});
		}
	});
	//移除所有筛选条件
	$(document).on("click", "a.removeAllFilter", function(){
		$.getJSON('/filters/remove_all_filter.json', function(data){
			ik.submitFilter();
		});
	});
}

//点击 应用筛选 时的事件
ik.filterApply=function(){

	//属性过滤
	$('.attr-filter-apply').change(function(){
		if( 'checked' == $(this).attr('checked')){
			var attrId = parseInt( $(this).data('attrid') );
			var rightdiv = $(this).parent().parent();
			var istext = rightdiv.data('istext');
			var welldiv = rightdiv.parent().parent();
			$('#RequestFilterQAttributeid').val( attrId );
			$('#RequestFilterQCategoryname').val( welldiv.find('h4').html().trim() );
			if(istext){
				//文本选择项
				var isSelected = false;
				var queryText = '';
				welldiv.find('input.textgroup').each(function(i,ele){
					if( $(ele).attr('checked') == 'checked'){
						isSelected = true;
						queryText = queryText + $(ele).val() + ',';
					}
				});
				if(isSelected){

				}else{
					alert('请至少选择1项继续筛选过滤');
					$(this).attr('checked', false) ;
					return;
				}

				$('#RequestFilterQIsText').val('1');
				$('#RequestFilterQText').val(queryText);
				ik.submitFilter();
				return;

			}else{
				if( welldiv.find('.filter_exact').attr('checked') == 'checked' ||   welldiv.find('.filter_range').attr('checked') == 'checked'  ){
					if(welldiv.find('.filter_exact').attr('checked') == 'checked'){
						//精确数值
						var v = $('#exact_' + attrId).val().trim();
						if( isNaN(parseFloat(v)) ){
							alert('请填写正确的 Exact 数值');
							$(this).attr('checked', false) ;
							$('#exact_' + attrId).focus();
							return;
						}else{
							$('#RequestFilterQIsText').val('0');
							$('#RequestFilterQIsExact').val('1');
							$('#RequestFilterQExactValue').val(parseFloat(v));
							$('#RequestFilterQExactUnit').val(  welldiv.find('.attribute_exact_unit').val() );
							$('#RequestFilterQUnit').val(  welldiv.find('.attribute_unit').val() );
							ik.submitFilter();
							return;
						}
					}else{
						//区间数值
						var v = $('#attr_min_' + attrId).val().trim();
						if( isNaN(parseFloat(v)) ){
							alert('请填写正确的 min 数值');
							$(this).attr('checked', false) ;
							$('#attr_min_' + attrId).focus();
							return;
						}
						$('#RequestFilterQMinValue').val(parseFloat(v));

						v = $('#attr_max_' + attrId).val().trim();
						if( isNaN(parseFloat(v)) ){
							alert('请填写正确的 max 数值');
							$(this).attr('checked', false) ;
							$('#attr_max_' + attrId).focus();
							return;
						}
						$('#RequestFilterQMaxValue').val(parseFloat(v));

						$('#RequestFilterQIsText').val('0');
						$('#RequestFilterQIsExact').val('0');
						$('#RequestFilterQUnit').val(  welldiv.find('.attribute_unit').val() );
						ik.submitFilter();
						return;
					}
				}else{
					alert('请选择 或者 Exact 或 Range');
					 $(this).attr('checked', false) ;
					 return;
				}
			}
		}
	});

	//厂牌过滤
	$('.manufacturer-filter-apply').change(function(){
		if( 'checked' == $(this).attr('checked')){
			var rightdiv = $(this).parent().parent();
			var welldiv = rightdiv.parent().parent();
			var isSelected = false;
			var queryValue = '', queryText = '';
			welldiv.find('input.textgroup').each(function(i,ele){
				if( $(ele).attr('checked') == 'checked'){
					isSelected = true;
					queryValue = queryValue + $(ele).val() + ',';
					queryText = queryText + $(ele).parent().text() + ',';
				}
			});
			if(isSelected){

			}else{
				alert('请至少选择1项');
				$(this).attr('checked', false) ;
				return;
			}
			$('#RequestFilterQManufacturers').val( queryValue );			
			if( queryText.length  > 20){
				queryText = queryText.substr(0,20) + "...";
			}
			$.cookie('TextQManufacturers', queryText);
			ik.submitFilter();
			return;
		}
	});

	//供应商过滤
	$('.supplier-filter-apply').change(function(){
		if( 'checked' == $(this).attr('checked')){
			var rightdiv = $(this).parent().parent();
			var welldiv = rightdiv.parent().parent();
			var isSelected = false;
			var queryValue = '', queryText = '';
			welldiv.find('input.textgroup').each(function(i,ele){
				if( $(ele).attr('checked') == 'checked'){
					isSelected = true;
					queryValue = queryValue + $(ele).val() + ',';
					queryText = queryText + $(ele).parent().text() + ',';
				}
			});
			if(isSelected){

			}else{
				alert('请至少选择1项');
				$(this).attr('checked', false) ;
				return;
			}
			
			$('#RequestFilterQSuppliers').val( queryValue );			
			if( queryText.length  > 20){
				queryText = queryText.substr(0,20) + "...";
			}
			$.cookie('TextQSuppliers', queryText);			
			ik.submitFilter();
			return;
		}
	});
}

//点击 seemore 看更多的属性项
ik.attrSeeMore=function(){
	$('.attr_filter_seemore').click(function(){
		$(this).parent().find('.seemore').show();
		$(this).hide();
	});
}

//点击 查看更多报价，已废弃，目前 全显示
ik.priceSeeMore=function(){
$('#tbclassAuthorized').click(function(){
	if("seemore" == $(this).data('class')){
		$('table.tbAuthorized th.seemore').addClass('seeless').removeClass('seemore');
		$('table.tbAuthorized td.seemore').addClass('seeless').removeClass('seemore');
		$(this).data('class', 'seeless').html('隐藏更多报价');
	}else{
		$('table.tbAuthorized th.seeless').addClass('seemore').removeClass('seeless');
		$('table.tbAuthorized td.seeless').addClass('seemore').removeClass('seeless');
		$(this).data('class', 'seemore').html('显示更多报价');
	}
});
}

ik.testFormKeyword=function(){
	var k = $('#txtKeyword').val().trim();
	if( k.length < 1){
		alert('输入关键字');
		return false;
	}
	$('#RequestFilterQKeyword').val( k );
	ik.submitFilter();
	return false;
};

