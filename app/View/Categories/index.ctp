<div class="container">
	<h1><?php echo $category['Category']['nodename']?></h1>
	<?php
	echo $this->Html->link('浏览全部 '.$category['Category']['title_cn'], array('controller'=>'parts', 'action'=>'category', $category['Category']['id']));
	?>
	<?php 
	foreach($categories as $key=>$category){
		if($key%2==0){
			echo '<div class="row">';
		}
	?>
		<div class="span6">
			<h3><?php 
			if($category['Category']['image_40px']){
				echo $this->Html->image($category['Category']['image_40px']);
			}
			if($category['Category']['children_ids']){
				echo $this->Html->link($category['Category']['title_cn'].'('.$category['Category']['num_parts'].')',
					array('controller'=>'categories', 'action'=>'index', $category['Category']['id']));
			}else{
				echo $this->Html->link($category['Category']['title_cn'].'('.$category['Category']['num_parts'].')',
					array('controller'=>'parts', 'action'=>'category', $category['Category']['id']));
			}
			?></h3>
			<ul>
				<?php 
				foreach($category['ChildCategory'] as $child){
					echo '<li>';
					if($child['children_ids']){
						echo $this->Html->link($child['title_cn'].'('.$child['num_parts'].')',
							array('controller'=>'categories', 'action'=>'index', $child['id']));
					}else{
						echo $this->Html->link($child['title_cn'].'('.$child['num_parts'].')',
							array('controller'=>'parts', 'action'=>'category', $child['id']));
					}
					echo '</li>';
				}
				?>
			</ul>
		</div>
	<?php 
		if($key%2==1 || $key==count($categories)-1){
			echo '</div>';
		}
	}
	?>
</div>