<!DOCTYPE html>
<html lang="en">
	<?php echo $this->Html->charset(); ?>
	<title><?php echo $title_for_layout; ?></title>
	<?php
		echo $this->Html->meta('icon');
		//echo $this->Html->css('cake.generic');
		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('http://code.jquery.com/ui/1.8.17/themes/base/jquery-ui.css');
		echo $this->Html->script(array(
			//'http://code.jquery.com/jquery-1.7.2.min.js',
			'/js/jquery-1.7.2.min.js',
			'http://code.jquery.com/ui/1.8.18/jquery-ui.min.js',
		));
		echo $this->Html->script(array('bootstrap.min', 'functions.js', 'jquery.cookie'));
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<?php echo $this->element('header'); ?>
	<?php echo $this->Session->flash(); ?>
	<?php echo $this->fetch('content'); ?>
	<div id="footer">
	</div>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
