<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-cn">
	<?php echo $this->Html->charset(); ?>
	<title><?php echo $title_for_layout; ?></title>
	<meta  name="keywords" content="ICkey,IC代购,芯片代购,IC小批量代购,电子元器件IC代购,Digikey代购,Digi-key代购,e络盟IC代购,Mouser代购,Future代购,IC分销,IC代购平台,小批量芯片IC">
	<meta name="description" content="ICkey代购网致力于为国内用户提供小批量芯片(IC)国外现货代购服务,网站实时提供美国DIGIKEY、MOUSER、e络盟、Future等芯片(IC)供应商的IC现货信息。香港交货美金原价代购IC,国内交货人民币价格,在线实时交易代购IC,最快5天到货,是IC行业内目前最成熟稳定的小批量芯片(IC)代购平台。">
</head>
<body>
	<?php echo $this->element('header_iframe'); ?>
	<?php echo $this->Session->flash(); ?>
	<?php echo $this->fetch('content'); ?>
	<div id="footer">
	</div>	
</body>
</html>