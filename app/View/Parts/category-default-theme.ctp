
<div id='content' class='w950'>
	<div class="row">
		<div style="display:none;">
		<?php 
			// 过滤器使用的Form
			echo $this->Form->create('RequestFilter', array('id' => 'PartFilterForm', 'url' => array('controller' => 'filters', 'action' => 'set_filter')));
			echo $this->Form->input('q_categoryid', array('value' => $category['Category']["id"] ));
			echo $this->Form->input('q_keyword');
			//厂商、供应商过滤器
			echo $this->Form->input('q_manufacturers', array('value' => 0 ));			
			echo $this->Form->input('q_suppliers', array('value' => 0 ));
			//属性过滤器
			echo $this->Form->input('q_attributeid');
			echo $this->Form->input('q_is_text',  array('value' => 0));
			echo $this->Form->input('q_text',  array('value' => ''));
			echo $this->Form->input('q_is_exact',  array('value' => 0));
			echo $this->Form->input('q_exact_value');
			echo $this->Form->input('q_exact_unit');
			echo $this->Form->input('q_min_value');
			echo $this->Form->input('q_max_value');
			echo $this->Form->input('q_unit');
			echo $this->Form->input('q_categoryname');
			echo $this->Form->end();
		?>
		</div>
	</div>
	
	<div id="main" class="row">
		<div class="hua_left_box span5">
			<div class="well sidebar-nav">
				<ul class="category-nav nav nav-list">
				<li class="nav-header">
					<?php 
					echo $this->Html->link('商品类别',array('action'=>'index'), array('class'=>'dl'));
					if($category){
						echo '&gt;&gt; ';
						if( (4161 != $category['ParentCategory']["id"]) && (4161 != $category['Category']["id"]) ){
							echo $this->Html->link($category['ParentCategory']["title_cn"],array('controller'=>'parts', 'action'=>'category',$category['ParentCategory']["id"]), array('class'=>'dl'));
							echo '&gt;&gt; ';
						}
						echo $this->Html->link($category['Category']["title_cn"], array('controller'=>'parts', 'action'=>'category', $category['Category']['id']) , array('class'=>'dl'));
					}
					?>
				</li>
				<?php foreach($categories as $item){?>
				<li><?php echo$this->Html->link($item['Category']['title_cn'] . '(' . $item['Category']['num_parts'] . ')', 
					array('controller'=>'parts', 'action'=>'category', $item['Category']['id']))?></li>
				<?php }?>
				</ul>
			</div>

<?php 
if(sizeof($manufacturers) > 0 || sizeof($suppliers) > 0 || sizeof($part_attributes) > 0 ){
echo <<< END
        <div class="well attr_filter">
END;
        if(sizeof($manufacturers) > 0){
echo <<< END
        <div class="well filter_manufacturers">
          <h3>厂牌</h3>
		<div class="clearfix">
			<div class="pull-right"  >
				<label class="checkbox">
				<input type="checkbox"  class="manufacturer-filter-apply"  name="manufacturer-filter"  />
				应用过滤
				</label>
			</div>
		</div>
END;
          foreach($manufacturers as $manufacturer){
			echo '<label class="checkbox">';
			echo '<input type="checkbox" class="textgroup" name="manufacturer" value="'.$manufacturer['Brand']['id'].'"/>';
			echo $manufacturer['Brand']['displayname'];
			echo "</label>\n";
          }          
echo ' </div>';
        
        }
        
        if(sizeof($suppliers) > 0){
echo <<< END
        <div class="well filter_suppliers">
          <h3>供应商</h3>
		<div class="clearfix">
			<div class="pull-right"  >
				<label class="checkbox">
				<input type="checkbox"  class="supplier-filter-apply"  name="supplier-filter"  />
				应用过滤
				</label>
			</div>
		</div>
END;
          foreach($suppliers as $supplier){
            echo '<label class="checkbox">';
            echo '<input type="checkbox" class="textgroup" name="supplier" value="'.$supplier['Brand']['id'].'"/>';
                    echo $supplier['Brand']['displayname'];
                    echo "</label>\n";
          }
          
echo ' </div>';  
        }
        
        
		//属性筛选	
		foreach($part_attributes as $part_attribute){
				if( strtolower( $part_attribute['PartAttribute']['fieldname'] ) == 'packaging'){
					continue;
				}
				if( strtolower( $part_attribute['PartAttribute']['fieldname'] ) == 'case_package'){
					continue;
				}
				//零件族
				if( $part_attribute['PartAttribute']['id']  == 35){
					continue;
				}

			?>
				<div class="well filter_attributes"  id="well_attrid_<?php echo $part_attribute['PartAttribute']['id']; ?>">
				<h3><?php if($part_attribute['PartAttribute']['title_cn']!='') {echo $part_attribute['PartAttribute']['title_cn'];}else{echo $part_attribute['PartAttribute']['displayname']; }; ?></h3>
					<div class="clearfix">
						<div class="pull-right" data-attrid="<?php echo $part_attribute['PartAttribute']['id'] ; ?>" <?php if($part_attribute['PartAttribute']['type']=='text'){echo 'data-istext="1"';}else{ echo 'data-istext="0"';} ?>  >
							<label class="checkbox">
							<input type="checkbox"  class="attr-filter-apply" data-attrid="<?php echo $part_attribute['PartAttribute']['id'] ; ?>" name="attr_id_<?php echo strtolower($part_attribute['PartAttribute']['id']) ; ?>"  />
							应用过滤
							</label>
						</div>
					</div>
					<?php 
					if($part_attribute['PartAttribute']['type']=='text'){ 
						echo '<div class="filterOptions">';
						foreach($part_attribute['values'] as $value){
							echo '<label class="checkbox">';
							echo '<input type="checkbox" class="textgroup" name="att_', $part_attribute['PartAttribute']['fieldname'], '" value="',  $value,   '"/>';
							echo $value;
				  			echo "</label>\n";
						}
						echo '</div>';
					}else{ 
						//数值型单位，可能涉及到毫 微 纳 皮
						$att_min = $part_attribute['min'];
						$att_max = $part_attribute['max'];

						if($att_min > $att_max){
							$temp = $att_min;
							$att_min = $att_max;
							$att_max = $temp;
						}
						$att_unit = 2;
						// convert to 10E3
						if(abs($att_min) < 1.00){
							$att_min = $att_min * 1000;
							$att_max = $att_max * 1000;
							$att_unit = $att_unit * 2;
						}
						// convert to 10E6
						if(abs($att_min) < 1.00){
							$att_min = $att_min * 1000;
							$att_max = $att_max * 1000;
							$att_unit = $att_unit * 2;
						}
						// convert to 10E9
						if(abs($att_min) < 1.00){
							$att_min = $att_min * 1000;
							$att_max = $att_max * 1000;
							$att_unit = $att_unit * 2;
						}
						// convert to 10E12
						if(abs($att_min) < 1.00){
							$att_min = $att_min * 1000;
							$att_max = $att_max * 1000;
							$att_unit = $att_unit * 2;
						}

						echo '<input type="hidden" class="attribute_unit" name="unit_', $part_attribute['PartAttribute']['id'], '" value="', $att_unit,   '" />';

						if(2 == $att_unit	){
							$att_unit	 =  $part_attribute['PartAttribute']['unit_symbol'];
							$att_unit_text = false;
						}else if(4 == $att_unit	){
							$att_unit	 =  ' m' . $part_attribute['PartAttribute']['unit_symbol'];
							$att_unit_text = '(10<sup>-3</sup> ' . $part_attribute['PartAttribute']['unit_symbol']  . ')';
						}else if(8 == $att_unit	){
							$att_unit	 =  ' μ' . $part_attribute['PartAttribute']['unit_symbol'];
							$att_unit_text = '(10<sup>-6</sup> ' . $part_attribute['PartAttribute']['unit_symbol']  . ')';
						}else if(16 == $att_unit	){
							$att_unit	 =  ' n' . $part_attribute['PartAttribute']['unit_symbol'];
							$att_unit_text = '(10<sup>-9</sup> ' . $part_attribute['PartAttribute']['unit_symbol']  . ')';
						}else if(32 == $att_unit	){
							$att_unit	 =  ' p' . $part_attribute['PartAttribute']['unit_symbol'];
							$att_unit_text = '(10<sup>-12</sup> ' . $part_attribute['PartAttribute']['unit_symbol']  . ')';
						}
						$att_min = sprintf("%0.3f", $att_min);
						$att_max = sprintf("%0.3f", $att_max);

echo <<<END
						<div class="filterOptions">
							<label class="checkbox">
								<input type="checkbox" class="filter_exact siggroup" name="filter_exact_{$part_attribute['PartAttribute']['id']}" value=""/>Set Exact:<br/>
							</label>
							Value:<input type="input" id="exact_{$part_attribute['PartAttribute']['id']}" name="attr_{$part_attribute['PartAttribute']['id']}" value=""/>
END;
							if( $att_unit_text){
								echo $att_unit_text ;
								echo '<input type="hidden" class="attribute_exact_unit" name="exact_unit_', $part_attribute['PartAttribute']['id'], '" value="', $att_unit_text,   '" />';								
							} ;
echo <<<END
							<label class="checkbox">
								<input type="checkbox" class="filter_range siggroup" name="attr_r_filter_range_{$part_attribute['PartAttribute']['id']}" value="">Set Range:<br/>
							</label>
							Min:<input type="input" id="attr_min_{$part_attribute['PartAttribute']['id']}" name="attr_min_{$part_attribute['PartAttribute']['id']}" value="{$att_min}"/>  {$att_unit} <br/>
							Max:<input type="input" id="attr_max_{$part_attribute['PartAttribute']['id']}" name="attr_max_{$part_attribute['PartAttribute']['id']}" value="{$att_max}"/> {$att_unit} <br/>
							<div id="slider_{$part_attribute['PartAttribute']['id']}" style="width:100px"></div>
						</div><!-- filterOptions -->

END;
          echo  '<script type="text/javascript">$(function(){
            $("#slider_'.$part_attribute['PartAttribute']['id'].
              '").slider({
              min:'.$att_min.',
              max:'.$att_max.',
              range: true,values:['.$part_attribute['min'].','.$part_attribute['max'].'],
              slide:function(event,ui){
                $("#attr_min_'.$part_attribute['PartAttribute']['id'].'").val(ui.values[0]);
                $("#attr_max_'.$part_attribute['PartAttribute']['id'].'").val(ui.values[1])
              }
            })
          });</script>';
          

					} ?>
				</div>
		<?php 
		}//foreach($part_attributes
	
		?>
		</div>
<?php 
}
?>

		</div><!-- hua_left_box span5 -->
		<div class="right_box span7">
			<div class="currentFilter" id="currentFilter" >
				<h3>当前的筛选条件</h3>
			</div>
			<?php if(!empty($parts) ) {?>
			<table class="table table-condensed">
				<thead>
					<tr>
						<th>型号名</th>
						<th>价格(均价)</th>
						<th>存货量</th>
						<th>供应商数量</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($parts as $part){?>
					<tr>
						<td><?php echo $this->Html->link($part['Part']['mpn'], 
							array('controller'=>'parts', 'action'=>'view', $part['Part']['id']))?></td>
						<td><?php 
							if($part['Part']['avg_price'] > 0.000){
								echo sprintf("%.3f", $part['Part']['avg_price']);
							}else{
								echo 'RFQ';
							}
							echo ' &nbsp;', $part['Part']['avg_price_unit'];
							?>
						</td>
						<td>
							<?php 
							$market_status = $part['Part']['market_status'];
							if(null == $market_status || strlen($market_status) < 5){
								echo '不足或未明';
							}else{
								if(1 == strpos($market_status, 'OOD')){
									echo '充足';
								}else if(1 == strpos($market_status, 'ARNING')){
									echo '少量';
								}
							}
							//if($part['Part']['avg_avail'] > 0.000){echo sprintf("%.3f", $part['Part']['avg_avail']);}else{echo '不足';}
							?>
						</td>
						<td><?php if($part['Part']['num_suppliers'] > 0){echo $part['Part']['num_suppliers'];}?></td>
					</tr>
					<?php }?>
				</tbody>
			</table>
			<p>
			<?php
			if(isset($this->Paginator) ){
				echo $this->Paginator->counter(array(
				'format' => __('第{:page}页 / {:pages} 页, 显示 {:current} 条记录  总计 {:count} , 开始于 {:start}, 结束于 {:end}')
				));
			}
			?>
			</p>

			<div class="pagination">
			<?php
				if(isset($this->Paginator) ){
					echo $this->Paginator->prev('< ', array(), null, array('class' => 'prev disabled'));
					echo $this->Paginator->numbers(array('separator' => '', 'currentClass'=>'active'));
					echo $this->Paginator->next(' >', array(), null, array('class' => 'next disabled'));
				}
			?>
			</div>

		<?php 
		}else{
			echo '<div><p>No item found! </p></div>';
		}//if(!empty($parts) )
		?>
		</div><!-- right_box span7 -->
	</div>
</div>

<?php 
if(!empty($parts) ) {
	// js 取动态实时价格
	// ready get dynamic price from octo
	$multi_ids = array();
	foreach($parts as $part){
		$multi_ids[] = $part['Part']['id'];
	}
	if( count($multi_ids) > 0){
		$str = implode($multi_ids,',');
		echo <<<END
			<script type="text/javascript">
				ik.reqMultiIds( "[  {$str} ]" );
			</script>
END;
	}
}

if($sessionFilterCondition ) {
	echo '<script type="text/javascript"> var currentFilter =';
	echo json_encode($sessionFilterCondition);
	echo ';</script>';
}
?>

<script type="text/javascript">
	$(document).ready(function(){
		ik.cateInit();
	});
</script>