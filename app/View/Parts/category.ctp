<?php
function avaril_to_string($avail){
	switch ($avail){
		case -1:
			return "non-stocked";
		case -2:
			return "yes";
		case -3:
			return "unknown";
		case -4:
			return "RFQ";
	}

	return $avail;
}
?>
<div id='content' class="w950 clearfix">
	<div id="main" class="row">
		<div class="hua_left_box span5">
			<div class="well sidebar-nav">
				<ul class="category-nav nav nav-list">
				<li class="nav-header">
					<?php 
					echo $this->Html->link('商品类别',array('action'=>'index'), array('class'=>'dl'));
					if($category){
						echo '&gt;&gt; ';
						if( (4161 != $category['ParentCategory']["id"]) && (4161 != $category['Category']["id"]) ){
							echo $this->Html->link($category['ParentCategory']["title_cn"],array('controller'=>'parts', 'action'=>'category',$category['ParentCategory']["id"]), array('class'=>'dl'));
							echo '&gt;&gt; ';
						}
						echo $this->Html->link($category['Category']["title_cn"], array('controller'=>'parts', 'action'=>'category', $category['Category']['id']) , array('class'=>'dl'));
					}
					?>
				</li>
				<?php foreach($categories as $item){?>
				<li><?php echo$this->Html->link($item['Category']['title_cn'] , 
					array('controller'=>'parts', 'action'=>'category', $item['Category']['id']))?></li>
				<?php }?>
				</ul>
			</div>

<?php 
if(sizeof($manufacturers) > 0 || sizeof($suppliers) > 0 || sizeof($part_attributes) > 0 ){
echo <<< END
        <div class="well attr_filter">
END;
        if(sizeof($manufacturers) > 0){
echo <<< END
        <div class="well filter_manufacturers">
          <h4>筛选厂牌</h4>
		<div class="clearfix">
			<div class="pull-right"  >
				<label class="checkbox">
				<input type="checkbox"  class="manufacturer-filter-apply"  name="manufacturer-filter"  />
				应用筛选
				</label>
			</div>
		</div>
END;
		$i_max_show_screen = 9;
		$i_show_screen = 0;
          foreach($manufacturers as $manufacturer){          
          	if($i_show_screen < $i_max_show_screen){
          		$i_show_screen ++;
          		echo '<label class="checkbox">';
          	}else{
          		echo '<label class="checkbox seemore">';
          	}
			echo '<input type="checkbox" class="textgroup" name="manufacturer" value="'.$manufacturer['Brand']['id'].'"/>';
			echo $manufacturer['Brand']['displayname'];
			echo "</label>\n";
          }
          if($i_show_screen >= $i_max_show_screen){
          	echo '<a href="#1" class="attr_filter_seemore">See More</a>';
          }
echo ' </div>';
        
        }
        
        if(sizeof($suppliers) > 0){
echo <<< END
        <div class="well filter_suppliers">
          <h4>筛选供应商</h4>
		<div class="clearfix">
			<div class="pull-right"  >
				<label class="checkbox">
				<input type="checkbox"  class="supplier-filter-apply"  name="supplier-filter"  />
				应用筛选
				</label>
			</div>
		</div>
END;

		$i_show_screen = 0;
		foreach($suppliers as $supplier){
          	if($i_show_screen < $i_max_show_screen){
          		$i_show_screen ++;
          		echo '<label class="checkbox">';          
          	}else{
          		echo '<label class="checkbox seemore">';
          	}
			echo '<input type="checkbox" class="textgroup" name="supplier" value="'.$supplier['Brand']['id'].'"/>';
			echo $supplier['Brand']['displayname'];
			echo "</label>\n";
          }
          if($i_show_screen >= $i_max_show_screen){
          	echo '<a href="#1" class="attr_filter_seemore">See More</a>';
          }
echo ' </div>';  
        }
        
        
		//属性筛选
		foreach($part_attributes as $part_attribute){
				if( strtolower( $part_attribute['PartAttribute']['fieldname'] ) == 'packaging'){
					continue;
				}
				if( strtolower( $part_attribute['PartAttribute']['fieldname'] ) == 'case_package'){
					continue;
				}
				//零件族
				if( $part_attribute['PartAttribute']['id']  == 35){
					continue;
				}
				if( $part_attribute['PartAttribute']['type']=='text' && (!$part_attribute['values'] ) ){
					continue;
				}

			?>
				<div class="well filter_attributes"  id="well_attrid_<?php echo $part_attribute['PartAttribute']['id']; ?>">
				<h4><?php if($part_attribute['PartAttribute']['title_cn']!='') {echo $part_attribute['PartAttribute']['title_cn'];}else{echo $part_attribute['PartAttribute']['displayname']; }; ?></h4>
					<div class="clearfix">
						<div class="pull-right" data-attrid="<?php echo $part_attribute['PartAttribute']['id'] ; ?>" <?php if($part_attribute['PartAttribute']['type']=='text'){echo 'data-istext="1"';}else{ echo 'data-istext="0"';} ?>  >
							<label class="checkbox">
							<input type="checkbox"  class="attr-filter-apply" data-attrid="<?php echo $part_attribute['PartAttribute']['id'] ; ?>" name="attr_id_<?php echo strtolower($part_attribute['PartAttribute']['id']) ; ?>"  />
							应用筛选
							</label>
						</div>
					</div>
					<?php 
					if($part_attribute['PartAttribute']['type']=='text'){ 
						echo '<div class="filterOptions">';
						$i_show_screen = 0;
						foreach($part_attribute['values'] as $value){
							if($i_show_screen < $i_max_show_screen){
				          		$i_show_screen ++;
				          		echo '<label class="checkbox">';
				          	}else{
				          		echo '<label class="checkbox seemore">';
				          	}
							echo '<input type="checkbox" class="textgroup" name="att_', $part_attribute['PartAttribute']['fieldname'], '" value="',  $value,   '"/>';
							echo $value;
				  			echo "</label>\n";
						}
						if($i_show_screen >= $i_max_show_screen){
          					echo '<a href="#1" class="attr_filter_seemore">See More</a>';
          				}
						echo '</div>';
					}else{ 
						//数值型单位，可能涉及到毫 微 纳 皮
						$att_min = $part_attribute['min'];
						$att_max = $part_attribute['max'];

						if($att_min > $att_max){
							$temp = $att_min;
							$att_min = $att_max;
							$att_max = $temp;
						}
						$att_unit = 2;
						// convert to 10E3
						if(abs($att_min) < 1.00){
							$att_min = $att_min * 1000;
							$att_max = $att_max * 1000;
							$att_unit = $att_unit * 2;
						}
						// convert to 10E6
						if(abs($att_min) < 1.00){
							$att_min = $att_min * 1000;
							$att_max = $att_max * 1000;
							$att_unit = $att_unit * 2;
						}
						// convert to 10E9
						if(abs($att_min) < 1.00){
							$att_min = $att_min * 1000;
							$att_max = $att_max * 1000;
							$att_unit = $att_unit * 2;
						}
						// convert to 10E12
						if(abs($att_min) < 1.00){
							$att_min = $att_min * 1000;
							$att_max = $att_max * 1000;
							$att_unit = $att_unit * 2;
						}

						echo '<input type="hidden" class="attribute_unit" name="unit_', $part_attribute['PartAttribute']['id'], '" value="', $att_unit,   '" />';

						if(2 == $att_unit	){
							$att_unit	 =  $part_attribute['PartAttribute']['unit_symbol'];
							$att_unit_text = false;
						}else if(4 == $att_unit	){
							$att_unit	 =  ' m' . $part_attribute['PartAttribute']['unit_symbol'];
							$att_unit_text = '(10<sup>-3</sup> ' . $part_attribute['PartAttribute']['unit_symbol']  . ')';
						}else if(8 == $att_unit	){
							$att_unit	 =  ' μ' . $part_attribute['PartAttribute']['unit_symbol'];
							$att_unit_text = '(10<sup>-6</sup> ' . $part_attribute['PartAttribute']['unit_symbol']  . ')';
						}else if(16 == $att_unit	){
							$att_unit	 =  ' n' . $part_attribute['PartAttribute']['unit_symbol'];
							$att_unit_text = '(10<sup>-9</sup> ' . $part_attribute['PartAttribute']['unit_symbol']  . ')';
						}else if(32 == $att_unit	){
							$att_unit	 =  ' p' . $part_attribute['PartAttribute']['unit_symbol'];
							$att_unit_text = '(10<sup>-12</sup> ' . $part_attribute['PartAttribute']['unit_symbol']  . ')';
						}
						$att_min = sprintf("%0.3f", $att_min);
						$att_max = sprintf("%0.3f", $att_max);

echo <<<END
						<div class="filterOptions">
							<label class="checkbox">
								<input type="checkbox" class="filter_exact siggroup" name="filter_exact_{$part_attribute['PartAttribute']['id']}" value=""/>Set Exact:<br/>
							</label>
							Value:<input type="input" id="exact_{$part_attribute['PartAttribute']['id']}" name="attr_{$part_attribute['PartAttribute']['id']}" value=""/>
END;
							if( $att_unit_text){
								echo $att_unit_text ;
								echo '<input type="hidden" class="attribute_exact_unit" name="exact_unit_', $part_attribute['PartAttribute']['id'], '" value="', $att_unit_text,   '" />';
							} ;
echo <<<END
							<label class="checkbox">
								<input type="checkbox" class="filter_range siggroup" name="attr_r_filter_range_{$part_attribute['PartAttribute']['id']}" value="">Set Range:<br/>
							</label>
							Min:<input type="input" id="attr_min_{$part_attribute['PartAttribute']['id']}" name="attr_min_{$part_attribute['PartAttribute']['id']}" value="{$att_min}"/>  {$att_unit} <br/>
							Max:<input type="input" id="attr_max_{$part_attribute['PartAttribute']['id']}" name="attr_max_{$part_attribute['PartAttribute']['id']}" value="{$att_max}"/> {$att_unit} <br/>
							<div id="slider_{$part_attribute['PartAttribute']['id']}" style="width:100px"></div>
						</div><!-- filterOptions -->

END;
          echo  '<script type="text/javascript">$(function(){
            $("#slider_'.$part_attribute['PartAttribute']['id'].
              '").slider({
              min:'.$att_min.',
              max:'.$att_max.',
              range: true,values:['.$part_attribute['min'].','.$part_attribute['max'].'],
              slide:function(event,ui){
                $("#attr_min_'.$part_attribute['PartAttribute']['id'].'").val(ui.values[0]);
                $("#attr_max_'.$part_attribute['PartAttribute']['id'].'").val(ui.values[1])
              }
            })
          });</script>';
          

					} ?>
				</div>
		<?php 
		}//foreach($part_attributes

		?>
		</div>
<?php 
}
?>

		</div><!-- hua_left_box span5 -->
		<div class="right_box span7">
			<div class="currentFilter" id="currentFilter" >
				<h4>当前的筛选条件</h4>
			</div>
			<?php 
			if(!empty($parts) ) {
				foreach($parts as $part){
					//商品图片
					echo '<div class="style1_box"><dl><dt><span class="partImage"><img onerror="this.src=\'/images/nophoto.png\';"  class="img" src="';
					if( count( $part['Image'] ) > 0){
						echo $part['Image'][0]['url_55px'];
					}else{
						echo '/images/nophoto.png';
					}
					echo '" /></span></dt>';

					//单页的链接和资料下载 
					echo '<dd><span class="pname">';
					$url_Datasheet = false;
					if( count( $part['Datasheet'] ) > 0){
						$url_Datasheet = $part['Datasheet'][0]['url'];
					}
					echo '<a href="/parts/view/', $part['Part']['id'] , '" target="_blank">' , $part['Part']['mpn'],  '</a></span>';
					echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					if($url_Datasheet ){
						echo '<a href="', $url_Datasheet , '"><span style="color:#7778cc; font-size:11px;">资料下载</span></a>';
						echo '<a href="/parts/view/', $part['Part']['id'] , '#downdatasheet" >' ;
						echo '<img src="/images/icon.gif"></a>';
					}
					echo '</dd>';

					//简短描述，详细的在 description 表中
					echo  '<dd>描述：',  htmlspecialchars( $part['Part']['short_description'])  , '</dd> ';


					echo  '<dd>' ;
					if($part['Part']['num_suppliers'] > 0){
						echo $part['Part']['num_suppliers'] , '家供应商';
					}else{
						echo '尚无供应商或未知';
					}					
					$market_status = $part['Part']['market_status'];
					echo ' 货源状态：' ;
					if(null == $market_status || strlen($market_status) < 5 || stripos($market_status, 'Out of Stock')){
						echo '<span style="color:red;">不足</span>';
					}else{
						if(1 == stripos($market_status, 'OOD')){
							echo '充足';
						}else if(1 == stripos($market_status, 'ARNING')){
							echo '<span style="color:red;">少量</span>';							
						}
					}
					echo ' &nbsp;&nbsp;平均价 ：$', number_format($part['Part']['avg_price'], 3);
					echo  '</dd> ';
					echo "</dl>\n";
					//供应列表
					if($part['Offer']){ ?>
						<table class="offTbl" id="oPart_<?php echo $part['Part']['id'];?>">
							<thead>
							<tr>
								<th>供应商</th>
								<th>型号</th>
								<th class="pavail">库存</th>
								<th>起订量</th>
								<th>价格</th>
								<th>操作</th>
							</tr>
							</thead>
						<?php
						foreach($part['Offer'] as $offer){  
							?>
							<tr class="preData">
							<td><img width="80" src="/images/loading2.gif" /></td>
							<td><?php echo $offer['sku'];?></td>							
							<td><?php echo avaril_to_string($offer['avail'])?></td>
							<td></td>
							<td></td>
							<td><?php echo'代购'; ?></td>
							</tr>
						<?php
						}
						echo "</table>\n";
					}
					echo "</div>\n";					
				}//foreach
				?>


			<p>
			<?php
			if(isset($this->Paginator) ){
				echo $this->Paginator->counter(array(
				'format' => __('第{:page}页 / {:pages} 页, 显示 {:current} 条记录  总计 {:count} , 开始于 {:start}, 结束于 {:end}')
				));
			}
			?>
			</p>

			<div class="pagination">
			<?php
				if(isset($this->Paginator) ){
					echo $this->Paginator->prev('< ', array(), null, array('class' => 'prev disabled'));
					echo $this->Paginator->numbers(array('separator' => '', 'currentClass'=>'active'));
					echo $this->Paginator->next(' >', array(), null, array('class' => 'next disabled'));
				}
			?>
			</div>


		<?php 
		}else{
			echo '<div><p>No item found! </p></div>';
		}//if(!empty($parts) )
		?>
		</div><!-- right_box span7 -->
	</div>
</div>

<?php 
if(!empty($parts) ) {
	// js 取动态实时价格
	// ready get dynamic price from octo
	$multi_ids = array();
	foreach($parts as $part){
		$multi_ids[] = $part['Part']['id'];
	}
	if( count($multi_ids) > 0){
		$str = implode($multi_ids,',');
		echo <<<END
			<script type="text/javascript">
				ik.reqMultiIds( "[  {$str} ]" );
			</script>
END;
	}
}

if($sessionFilterCondition ) {
	echo '<script type="text/javascript"> var currentFilter =';
	echo json_encode($sessionFilterCondition);
	echo ';</script>';
}
?>

<script type="text/javascript">
	$(document).ready(function(){
		ik.cateInit();
	});
</script>