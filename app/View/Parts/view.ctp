
<?php 
echo $this->Html->css('/css/index.css');

function avaril_to_string($avail){
	switch ($avail){
		case -1:
			return "non-stocked";
		case -2:
			return "yes";
		case -3:
			return "unknown";
		case -4:
			return "RFQ";
	}
		
	return $avail;
}
?>
<div id='content' class="w950 clearfix">
	<div class="row">
		<!--  div>http://210.14.69.137/pro_details.php?proid=879645</div  -->
		<ul class="breadcrumb">
			<li>
				<?php echo $this->Html->link('商品类别', array('controller'=>'parts', 'action'=>'index'));?>
				<span class="divider">/</span>		
			</li>
			<?php 
			foreach ($part['Category'] as $key=>$category){
				if($key==count($part['Category'])-1){
					echo '<li class="active">';
					echo $this->Html->link($category['title_cn'], array('controller'=>'parts', 'action'=>'category', $category['id']));
					echo '</li>';
				}else{
					echo '<li>';
					echo $this->Html->link($category['title_cn'], array('controller'=>'parts', 'action'=>'category', $category['id']));
					echo '<span class="divider">/</span>';
					echo '</li>';
				}
			}
			?>
		</ul>
	</div>
	
	<div id="product">
		<div class="box11">
			<img class="productMainImg"  src="
			<?php 
			if(is_array($part['Image']) && count($part['Image'] )> 0){				
				echo $part['Image'][0]['url_90px'];
			}else{
				echo '/images/nophoto.png';
			}	
			?>" />
		</div>
		<div class="box22">
			<dl>
				<dt id="pro_mno">
					<?php echo $part[ 'Part'][ 'mpn']?>
				</dt>
				<dd class="font12">
					<a href="#downdatasheet">下载链接</a>&nbsp;<img src="/images/icon.gif">
				</dd>
				<dd>
					制造商：
				</dd>
				<dd>
					描述：<?php echo htmlspecialchars( $part['Part']['short_description']) ;?>
				</dd>
				<dd>
					<span class="f2">
						<?php echo $total_seller; ?> 家供应商
					</span>
					&nbsp;&nbsp;平均价: 
					<span class="f2">
						$<?php echo number_format($part['Part']['avg_price'], 3); ?>
					</span>
				</dd>
				<dd>
					<?php echo count($authorized_seller_numbers); ?> 家认证经销商
					<?php echo count($non_authorized_seller_numbers); ?> 家在线零售商
					<?php echo count($non_authorized_dealers_numbers); ?> 家其他类型零售商
				</dd>
				<dd>
					<span class="red">仅对供应商"官网价格"进行比较,如提醒有代购操作费,则尚未分摊,请留意</span>
				</dd>
			</dl>
		</div>
	</div>

	<div  class="tab1">
		<a name="tbAuthorized"></a>		
		<div class="page-header"><h3 class="title">认证经销商(Authorized Distributors)</h3></div>
		<table class="table table-bordered tbAuthorized" >
			<thead>
				<tr>
					<th>供应商</th>
					<th>型号名</th>					
					<th class="pavail">库存</th>
					<?php					
					foreach($authorized_seller_numbers as $number){
						echo "<th class=\"pris\">{$number} 片</th>";
					}					
					?>
					<th>代购</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				foreach($part['Offer'] as $offer){
					if($offer['is_authorized']){
				?>
				<tr>
					<td><?php echo $offer['Brand']['displayname']?></td>
					<td><?php echo $this->Html->link($offer['sku'], $offer['clickthrough_url'], array('target'=>'_blank'))?></td>					
					<td><?php echo avaril_to_string($offer['avail'])?></td>
				<?php 
						$i_count = 0;
						foreach($authorized_seller_numbers as $number){
							$i_count ++;
							if(array_key_exists($number, $offer['prices'])){								
								echo"<td>".$offer['prices'][$number]."</td>\n";								
							}else{
								echo"<td>&nbsp;</td>\n";
							}
						}						
				?>
				<td><?php echo $this->Html->link('BuyNow', '#1') ?></td>
				</tr>
				<?php
					}
				}
				?>
			</tbody>
		</table>


<?php
if(count($non_authorized_seller_numbers)){
?>	

		<div class="page-header"><h3 class="title">在线零售商(Non-Authorized Online Resellers)</h3></div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>供应商</th>
					<th>型号名</th>					
					<th class="pavail">库存</th>
					<?php
					foreach($non_authorized_seller_numbers as $number){						
						echo "<th class=\"pris\">{$number} 片</th>";
					}
					?>
					<th>代购</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				foreach($part['Offer'] as $offer){
					if($offer['is_brokered']){
				?>
				<tr>
					<td><?php echo $offer['Brand']['displayname']?></td>
					<td><?php echo $this->Html->link($offer['sku'], $offer['clickthrough_url'], array('target'=>'_blank'))?></td>					
					<td><?php echo avaril_to_string($offer['avail'])?></td>
				<?php 
						foreach($non_authorized_seller_numbers as $number){
							if(array_key_exists($number, $offer['prices'])){
								echo"<td>".$offer['prices'][$number]."</td>\n";
							}else{
								echo"<td>&nbsp;</td>\n";
							}
						}
				?>
					<td><?php echo $this->Html->link('BuyNow', '#1') ?></td>
				</tr>
				<?php
					}
				}
				?>
			</tbody>
		</table>

<?php
}
?>	

<?php
if(count($non_authorized_dealers_numbers)){
?>	
		<div class="page-header"><h3 class="title">其他零售商(Non-Authorized Dealers)</h3></div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>供应商</th>
					<th>型号名</th>
					<th>代购</th>
					<th class="pavail">库存</th>
					<?php
					foreach($non_authorized_dealers_numbers as $number){
						echo "<th class=\"pris\">{$number} 片</th>";
					}
					?>
				</tr>
			</thead>
			<tbody>
				<?php 
				foreach($part['Offer'] as $offer){
					if(!$offer['is_authorized'] && !$offer['is_brokered']){
				?>
				<tr>
					<td><?php echo $offer['Brand']['displayname']?></td>
					<td><?php echo $this->Html->link($offer['sku'], $offer['clickthrough_url'], array('target'=>'_blank'))?></td>
					<td><?php echo $this->Html->link('BuyNow', '#1') ?></td>
					<td><?php echo avaril_to_string($offer['avail'])?></td>
				<?php 
						foreach($non_authorized_dealers_numbers as $number){
							if(array_key_exists($number, $offer['prices'])){
								echo"<td>".$offer['prices'][$number]."</td>\n";
							}else{
								echo"<td>&nbsp;</td>\n";
							}
						}
				?>
				</tr>
				<?php
					}
				}
				?>
			</tbody>
		</table>
<?php
}
?>		
	</div>
	
	<div>
		<div class="page-header"><h3 class="title">技术规格</h3></div>
		<table class="table">
			<?php foreach ($part['Spec'] as $spec){?>
			<tr>
				<td><?php echo $spec['PartAttribute']['title_cn'] ?></td>
				<td>
					<?php 
					if($spec['PartAttribute']['type'] == 'number'){
						echo $spec['floatvalue'] . ' ' . $spec['PartAttribute']['unit_name'] ;
					}else{
						echo $spec['value'] . ' ' . $spec['PartAttribute']['unit_name'] ;
					}
					
					?>
				</td>
			</tr>
			<?php }?>
		</table>
	</div>
	
	<div>
		<div class="page-header"><h3 class="title">描述</h3></div>
		<?php foreach ($part['Description'] as $description){?>
			<h3></h3>
			<p><?php echo $description['text']; ?><br/>
			<?php echo $this->Html->link($description['credit_domain'], $description['credit_url']);?>
			</p>
		<?php }?>
	</div>
	
	
	<a name="downdatasheet"></a>
	<div>
		<div class="page-header"><h3 class="title">Datasheets</h3></div>
		<?php
		if($part['Datasheet']){
			echo '<ul>';
			foreach($part['Datasheet'] as $datasheet){
		?>
			<li><?php echo $this->Html->link($datasheet['url'], $datasheet['url']) ?> </li>
		<?php
			}
			echo '</ul>';
		}
		?>
	</div>
	
	<div>
		<?php if(is_array($part['Image'])){?>
		<div class="page-header"><h3 class="title">图片</h3></div>
		<?php foreach($part['Image'] as $image){?>
			<?php echo $this->Html->image($image['url_90px'], array('url'=>$image['url']))?>
		<?php }?>
		<?php
		}
		?>
	</div>
	
	<div>
		<br/><br/>
	</div>
</div>
