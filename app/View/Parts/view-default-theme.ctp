
<style>
	th.seemore{display:none;}
	td.seemore{display:none;}
</style>

<?php 
$i_count_max = 6;
function avaril_to_string($avail){
	switch ($avail){
		case -1:
			return "non-stocked";
		case -2:
			return "yes";
		case -3:
			return "unknown";
		case -4:
			return "RFQ";
	}
		
	return $avail;
}
?>
<div class="container">
	<div class="row">
		<ul class="breadcrumb">
			<li>
				<?php echo $this->Html->link('商品类别', array('controller'=>'parts', 'action'=>'index'));?>
				<span class="divider">/</span>		
			</li>
			<?php 
			foreach ($part['Category'] as $key=>$category){
				if($key==count($part['Category'])-1){
					echo '<li class="active">';
					echo $this->Html->link($category['title_cn'], array('controller'=>'parts', 'action'=>'category', $category['id']));
					echo '</li>';
				}else{
					echo '<li>';
					echo $this->Html->link($category['title_cn'], array('controller'=>'parts', 'action'=>'category', $category['id']));
					echo '<span class="divider">/</span>';
					echo '</li>';
				}
			}
			?>
		</ul>
	</div>
	
	<div>
		<h1><?php echo $part['Part']['mpn']?></h1>
	</div>

	<div >
		<a name="tbAuthorized"></a>
		<div class="page-header"><h2>Compare Suppliers</h2></div>		
		<h3>认证经销商(Authorized Distributors)</h3>		
		<table class="table table-bordered tbAuthorized" >
			<thead>
				<tr>
					<th>供应商</th>
					<th>型号名</th>
					<th>代购</th>
					<th>库存</th>
					<?php
					$i_count = 0;
					foreach($authorized_seller_numbers as $number){
						$i_count ++;
						if( $i_count < $i_count_max){
							echo "<th>{$number} 片</th>";
						}else{
							echo "<th class=\"seemore\">{$number} 片</th>";
						}
					}
					?>
				</tr>
			</thead>
			<tbody>
				<?php 
				foreach($part['Offer'] as $offer){
					if($offer['is_authorized']){
				?>
				<tr>
					<td><?php echo $offer['Brand']['displayname']?></td>
					<td><?php echo $this->Html->link($offer['sku'], $offer['clickthrough_url'], array('target'=>'_blank'))?></td>
					<td><?php echo $offer['buynow_url']?$this->Html->link('Buy now', $offer['buynow_url'], array('target'=>'_blank')):'';?></td>
					<td><?php echo avaril_to_string($offer['avail'])?></td>
				<?php 
						$i_count = 0;
						foreach($authorized_seller_numbers as $number){
							$i_count ++;
							if(array_key_exists($number, $offer['prices'])){
								if( $i_count < $i_count_max){
									echo"<td>".$offer['prices'][$number]."</td>\n";
								}else{
									echo"<td class=\"seemore\">".$offer['prices'][$number]."</td>\n";									
								}
							}else{
								if( $i_count < $i_count_max){
									echo"<td>&nbsp;</td>\n";
								}else{
									echo"<td class=\"seemore\">&nbsp;</td>\n";
								}
							}
						}
				?>
				</tr>
				<?php
					}
				}
				?>
			</tbody>
		</table>
		<?php
		if($i_count < $i_count_max){
		}else{
		?>
			<div>
				<a id="tbclassAuthorized" data-class="seemore" href="#tbAuthorized" title="阶梯报价">显示更多报价</a>
				<br/><br/><?php echo $octTheme;?></><br/>
			</div>
			<script>
				$('#tbclassAuthorized').click(function(){
					if("seemore" == $(this).data('class')){
						$('table.tbAuthorized th.seemore').addClass('seeless').removeClass('seemore');
						$('table.tbAuthorized td.seemore').addClass('seeless').removeClass('seemore');
						$(this).data('class', 'seeless').html('隐藏更多报价');
					}else{
						$('table.tbAuthorized th.seeless').addClass('seemore').removeClass('seeless');
						$('table.tbAuthorized td.seeless').addClass('seemore').removeClass('seeless');
						$(this).data('class', 'seemore').html('显示更多报价');
					}
				});
			</script>
		<?php
		}
		?>
		
		<h3>在线零售商(Non-Authorized Online Resellers)</h3>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>供应商</th>
					<th>型号名</th>
					<th>代购</th>
					<th>库存</th>
					<?php
					foreach($non_authorized_seller_numbers as $number){
						echo"<th>{$number} 片</th>";
					}
					?>
				</tr>
			</thead>
			<tbody>
				<?php 
				foreach($part['Offer'] as $offer){
					if($offer['is_brokered']){
				?>
				<tr>
					<td><?php echo $offer['Brand']['displayname']?></td>
					<td><?php echo $this->Html->link($offer['sku'], $offer['clickthrough_url'], array('target'=>'_blank'))?></td>
					<td><?php echo $offer['buynow_url']?$this->Html->link('Buy now', $offer['buynow_url'], array('target'=>'_blank')):'';?></td>
					<td><?php echo avaril_to_string($offer['avail'])?></td>
				<?php 
						foreach($non_authorized_seller_numbers as $number){
							if(array_key_exists($number, $offer['prices'])){
								echo"<td>".$offer['prices'][$number]."</td>\n";
							}else{
								echo"<td>&nbsp;</td>\n";
							}
						}
				?>
				</tr>
				<?php
					}
				}
				?>
			</tbody>
		</table>
		
		<h3>其他零售商(Non-Authorized Dealers)</h3>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>供应商</th>
					<th>型号名</th>
					<th>代购</th>
					<th>库存</th>
					<?php
					foreach($non_authorized_dealers_numbers as $number){
						echo"<th>{$number} 片</th>";
					}
					?>
				</tr>
			</thead>
			<tbody>
				<?php 
				foreach($part['Offer'] as $offer){
					if(!$offer['is_authorized'] && !$offer['is_brokered']){
				?>
				<tr>
					<td><?php echo $offer['Brand']['displayname']?></td>
					<td><?php echo $this->Html->link($offer['sku'], $offer['clickthrough_url'], array('target'=>'_blank'))?></td>
					<td><?php echo $offer['buynow_url']?$this->Html->link('Buy now', $offer['buynow_url'], array('target'=>'_blank')):'';?></td>
					<td><?php echo avaril_to_string($offer['avail'])?></td>
				<?php 
						foreach($non_authorized_dealers_numbers as $number){
							if(array_key_exists($number, $offer['prices'])){
								echo"<td>".$offer['prices'][$number]."</td>\n";
							}else{
								echo"<td>&nbsp;</td>\n";
							}
						}
				?>
				</tr>
				<?php
					}
				}
				?>
			</tbody>
		</table>
		
	</div>
	
	<div>
		<div class="page-header"><h2>技术规格</h2></div>
		<table class="table">
			<?php foreach ($part['Spec'] as $spec){?>
			<tr>
				<td><?php echo $spec['PartAttribute']['title_cn'] ?></td>
				<td>
					<?php 
					if($spec['PartAttribute']['type'] == 'number'){
						echo $spec['floatvalue'] . ' ' . $spec['PartAttribute']['unit_name'] ;
					}else{
						echo $spec['value'] . ' ' . $spec['PartAttribute']['unit_name'] ;
					}
					
					?>
				</td>
			</tr>
			<?php }?>
		</table>
	</div>
	
	<div>
		<div class="page-header"><h2>描述</h2></div>
		<?php foreach ($part['Description'] as $description){?>
			<h3></h3>
			<p><?php echo $description['text']; ?><br/>
			<?php echo $this->Html->link($description['credit_domain'], $description['credit_url']);?>
			</p>
		<?php }?>
	</div>
	
	<div>
		<div class="page-header"><h2>Datasheets</'h2></div>
		<?php
		if($part['Datasheet']){
			echo '<ul>';
			foreach($part['Datasheet'] as $datasheet){
		?>
			<li><?php echo $this->Html->link($datasheet['url'], $datasheet['url']) ?> (score:<?php echo $datasheet['score']?>)</li>
		<?php
			}
			echo '</ul>';
		}
		?>
	</div>
	
	<div>
		<div class="page-header"><h2>图片</h2></div>
		<?php foreach($part['Image'] as $image){?>
			<?php echo $this->Html->image($image['url_90px'], array('url'=>$image['url']))?>
		<?php }?>
	</div>
	
	<div>
		<div class="page-header"><h2>&nbsp;</h2></div>
	</div>
</div>