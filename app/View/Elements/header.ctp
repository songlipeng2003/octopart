<div class="navbar">
	<div class="navbar-inner">
		<div class="container">
			<!-- Be sure to leave the brand out there if you want it shown -->
			<a class="brand" href="#">Part</a>

			<!-- Everything you want hidden at 940px or less, place within here -->
			<div class="nav-collapse">
	            <ul class="nav">
					<li<?php if($this->request->params['controller']=='parts'){?> class="active"<?php }?>>
						<?php echo $this->Html->link('Parts', array('controller'=>'parts', 'action'=>'index'))?>
					</li>
					<li<?php if($this->request->params['controller']=='categories'){?> class="active"<?php }?>>
						<?php echo $this->Html->link('Categories', array('controller'=>'categories', 'action'=>'index'))?>
					</li>
	            </ul>
	            <ul class="nav pull-right">
					<li><a href="#">Register</a></li>
					<li><a href="#">Login</a></li>
	            </ul>
          </div><!--/.nav-collapse -->
		</div>
	</div>
</div>
