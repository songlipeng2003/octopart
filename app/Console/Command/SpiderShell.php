<?php
App::uses('Shell', 'AppShell');
App::import('Lib', 'Web');

/**
 * @property Category $Category
 * @property Part $Part
 * @property PartAttribute $PartAttribute
 * @property PartContent $PartContent
 */
class SpiderShell extends AppShell{
	public $uses = array('Category', 'Part', 'PartAttribute', 'PartContent');
	
	public $cookies = array(
		'__oasid'=>'247136a6-9d74-4134-88c9-84da598e9213',
		'__oavid'=>'ba4db96e-6e2f-4422-9704-4752a0c689f5',
		'__qca'=>'P0-1637915345-1332311049139',
	);
	
	public $proxy = array(
		'host'=>'127.0.0.1',
		'port'=>'7071',
		'type'=>'socks5'
	);
	
	//public $proxy = false;
	
	public function main(){
		//$this->fetch_categories();
		//$this->fetch_part(39619421);
		//$this->fetch_part_url();
		//$this->fetch_parts();
		$this->process_part_contents();
	}
	
	private function fetch_part_url(){
		$categories = $this->Category->find('all', array(
			'conditions'=>array(
				'Category.children_ids'=>'',
				'Category.id >'=>5498
			)
		));
		
		$limit = 20;
		
		foreach ($categories as $category){
			$category_id = $category['Category']['id'];
			$start = 0;
			$url = "http://octopart.com/api/v2/parts/search?apikey=68b25f31&filters=%5B%5B%22category_id%22%2C%5B$category_id%5D%5D%5D&start=$start&limit=1&_=".rand(0, 1000);
			echo "url=$url\n";
			$content = web_fetch($url, array(
				'cookie'=>$this->cookies,
				'is_retry'=>true,
				'try_times'=>3,
				'proxy'=>$this->proxy
			));
			
			$json = json_decode($content, true);
			$hits = $json['hits'];
			echo "hits:$hits\n";
			if($hits==0){
				//如果没有匹配数据直接不保存url
				continue;
			}
			
			$url = "http://octopart.com/api/v2/parts/search?apikey=68b25f31&filters=%5B%5B%22category_id%22%2C%5B$category_id%5D%5D%5D&start=$start&limit=$limit&_=".rand(0, 1000);
			$part_content = array(
				'url'=>$url,
				'is_fetch'=>0,
				'is_process'=>0,
				'number'=>$hits>$limit?$limit:$hits
			);
			$this->PartContent->create();
			$this->PartContent->save($part_content);
			
			$start +=$limit;
			
			if($hits<=1000){
				while($start<$hits && $start<1000){
					$url = "http://octopart.com/api/v2/parts/search?apikey=68b25f31&filters=%5B%5B%22category_id%22%2C%5B$category_id%5D%5D%5D&start=$start&limit=$limit&_=".rand(0, 1000);
					echo "url=$url\n";
					$part_content = array(
						'url'=>$url,
						'is_fetch'=>0,
						'is_process'=>0,
						'number'=>$hits>$start+$limit?$limit:$hits-$start
					);
					$this->PartContent->create();
					$this->PartContent->save($part_content);
					$start +=$limit;
				}
			}else{
				$words = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
				$words = str_split($words);
				
				foreach($words as $word){
					$word .= '*';
					$start = 0;
					$url = "http://octopart.com/api/v2/parts/search?apikey=68b25f31&q=$word&filters=%5B%5B%22category_id%22%2C%5B$category_id%5D%5D%5D&start=$start&limit=1&_=".rand(0, 1000);
					echo "url=$url\n";
					$content = web_fetch($url, array(
						'cookie'=>$this->cookies,
						'is_retry'=>true,
						'try_times'=>3,
						'proxy'=>$this->proxy
					));
					$json = json_decode($content, true);
					$hits = $json['hits'];
					echo "hits:$hits\n";
					if($hits==0){
						//如果没有匹配数据直接不保存url
						continue;
					}
					
					$url = "http://octopart.com/api/v2/parts/search?apikey=68b25f31&q=$word&filters=%5B%5B%22category_id%22%2C%5B$category_id%5D%5D%5D&start=$start&limit=$limit&_=".rand(0, 1000);

					$part_content = array(
						'url'=>$url,
						'is_fetch'=>0,
						'is_process'=>0,
						'number'=>$hits>$limit?$limit:$hits
					);
					$this->PartContent->create();
					$this->PartContent->save($part_content);
					
					$start +=$limit;
					
					while($start<$hits && $start<1000){
						$url = "http://octopart.com/api/v2/parts/search?apikey=68b25f31&q=$word&filters=%5B%5B%22category_id%22%2C%5B$category_id%5D%5D%5D&start=$start&limit=$limit&_=".rand(0, 1000);
						echo "url=$url\n";
						$part_content = array(
							'url'=>$url,
							'is_fetch'=>0,
							'is_process'=>0,
							'number'=>$hits>$start+$limit?$limit:$hits-$start
						);
						$this->PartContent->create();
						$this->PartContent->save($part_content);
						$start +=$limit;
					}
				}
			}
		}
	}
	
	private function fetch_parts(){
		$part_contents = $this->PartContent->find('all', array(
			'conditions'=>array(
				'PartContent.is_fetch' => 0,
			),
			'limit'=>10000
		));
		
		if(!$part_contents){
			return;
		}
		
		foreach($part_contents as $part_content){
			$content = web_fetch($part_content['PartContent']['url'], array(
				'cookie'=>$this->cookies, 
				'is_retry'=>true,
				'try_times'=>3,
				'proxy'=>$this->proxy
			));
			echo $part_content['PartContent']['id'].' size of '.strlen($content)."\n";
			if($content){
				$part_content['PartContent']['content'] = $content;
				$part_content['PartContent']['is_fetch'] = true;
				$this->PartContent->save($part_content);
			}
		}
	}
	
	private function process_part_contents(){
		$this->PartContent->query(' SET FOREIGN_KEY_CHECKS=0 ');
		$part_contents = $this->PartContent->find('all', array(
			'conditions'=>array(
				'PartContent.is_fetch' => 1 ,
				'PartContent.is_process' => 0 ,
//        'PartContent.is_process'=> 0 ,
//        'PartContent.id <='=> 125288,
//        'PartContent.id >='=> 74701,
			),
			'limit'=>800
		));
		
		echo 'size:'.count($part_contents)."\n";
		
		if(!$part_contents){
			return;
		}
		
		$index = 0;
		foreach($part_contents as $part_content){
			$content = $part_content['PartContent']['content'];
			$json = json_decode($content, true);
			$index ++;
			if($json){
				echo "process part_content {$index} id= {$part_content['PartContent']['id']} \n";
				foreach($json['results'] as $result){
					$part = $result['item'];
					$this->save_part($part);
					echo "save part:".$part['uid']."\n";
					$part_content['PartContent']['is_process'] = true;
					//$this->PartContent->save($part_content);
					$this->PartContent->id = $part_content['PartContent']['id'];
					$this->PartContent->saveField('is_process', '1', false);
				}
			}else{
				//set content to empty sothat we can re-fetch it!
				echo 'json decode fail id=' , $part_content['PartContent']['id'], "\n";
				$this->PartContent->id = $part_content['PartContent']['id'];
				//make it re-fetch
				$this->PartContent->saveField('content', '' , false);
				$this->PartContent->saveField('is_fetch', '0', false);
			}
		}
	}
	
	private function fetch_categories(){
		$root_ids = array(4161);
		
		foreach($root_ids as $category_id){
			$this->fetch_category($category_id);
		}
	}
	
	
	private function get_json($url){
		$content = web_fetch($url);
		
		$json = json_decode($content, true);
		
		return $json;
	}
	
	private function fetch_category($category_id){
		$category = $this->Category->read(null, $category_id);
		if(!$category){
			$url = "http://octopart.com/api/v2/categories/get?apikey=68b25f31&id=$category_id";
				
			$json = $this->get_json($url);
			if(!$json){
				return;
			}
			sleep(1);
			
			$category['Category'] = $json;
			
			$category['Category']['ancestor_ids'] = implode(',', $json['ancestor_ids']);
			$category['Category']['children_ids'] = implode(',', $json['children_ids']);
			
			$category['Category']['image'] = $json['images'][0]['url'];
			$category['Category']['image_40px'] = $json['images'][0]['url_40px'];
			$category['Category']['image_50px'] = $json['images'][0]['url_50px'];
			
			if($this->Category->save($category)){
				$this->log("save category:".$category['Category']['nodename'].' successfully', LOG_DEBUG);
			}else{
				$this->log("save category:".$category['Category']['nodename'].' fail');
			}
		}
			
		if($category['Category']['children_ids']){
			$children_ids = explode(',', $category['Category']['children_ids']);
			foreach($children_ids as $category_id){
				$this->fetch_category($category_id);
			}
		}
	}
	
	private function fetch_part($part_id){
		$url = "http://octopart.com/api/v2/parts/get?apikey=68b25f31&uid=$part_id";
		
		$category = $this->get_json($url);
		
		$this->save_part($category);
	}
	
	private function save_part($json){
		$part = $this->Part->read(null, $json['uid']);
		if($part){
			return false;
		}
		
		$part['Part'] = $json;
		$part['Part']['id'] = $json['uid'];
		$part['Part']['avg_price'] = $json['avg_price'][0];
		$part['Part']['avg_price_unit'] = $json['avg_price'][1];
		$part['Part']['brand_id'] = $json['manufacturer']['id'];
		if($json['datasheets']){
			$part['Datasheet'] = $json['datasheets'];
		}
		if($json['descriptions']){
			$part['Description'] = $json['descriptions'];
		}
		if($json['images']){
			$part['Image'] = $json['images'];
		}
// 		if($json['hyperlinks']){
// 			$part['Hyperlink'] = $json['hyperlinks'];
// 		}
		
		unset($part['Part']['datasheets']);
		unset($part['Part']['descriptions']);
		unset($part['Part']['images']);
		unset($part['Part']['offers']);
		unset($part['Part']['specs']);
		unset($part['Part']['manufacturer']);
		unset($part['Part']['category_ids']);
		unset($part['Part']['hyperlinks']);
		unset($part['Part']['uid']);
		
		$brand = $json['manufacturer'];
		$this->Part->Brand->save($brand);
		
		if($json['category_ids']){
			$categories = $this->Part->Category->find('all',array(
				'conditions'=>array(
					'Category.id'=>$json['category_ids'],
				),
				'recursive' => -1
			));
			
			foreach($categories as &$category){
				$part['Category']['Category'][] = $category['Category']['id'];
			}
		}
		
		if($json['offers']){
			$part['Offer'] = $json['offers'];
			foreach($part['Offer'] as &$offer){
				$offer['brand_id'] = $offer['supplier']['id'];
				$this->Part->Brand->save($offer['supplier']);
				
				if($offer['prices']){
					foreach($offer['prices'] as $price){
						$offer_price['number'] = $price[0];
						$offer_price['price'] = $price[1];
						$offer_price['currency'] = $price[2];
						$offer['Price'][] = $offer_price;
					}
				}
				
				unset($offer['supplier']);
				unset($offer['prices']);
			}
		}
		
		if($json['specs']){
			foreach ($json['specs'] as $spec) {
				$display_name = $spec['attribute']['displayname'];
				
				$attribute = $this->PartAttribute->findByDisplayname($display_name);
				
				if(!$attribute){
					$attribute['PartAttribute'] = $spec['attribute'];
					if(isset($spec['attribute']['metadata'])){
						$attribute['PartAttribute']['datatype'] = $spec['attribute']['metadata']['datatype'];
						$attribute['PartAttribute']['unit_symbol'] = $spec['attribute']['metadata']['unit']['symbol'];
						$attribute['PartAttribute']['unit_name'] = $spec['attribute']['metadata']['unit']['name'];
					}
					unset($attribute['PartAttribute']['metadata']);
					$this->PartAttribute->create();
					$this->PartAttribute->save($attribute);
					
					$attribute = $this->PartAttribute->findByDisplayname($display_name);
				}
				
				if($spec['values']){
					foreach($spec['values'] as $value){
						if(is_array($value)){
							foreach($value as $name=>$val){
								if($val){
									$attr = array();
									$attr['part_attribute_id'] = $attribute['PartAttribute']['id'];
									//$attr['name'] = $name;
									if($attribute['PartAttribute']['type']=='number'){
										$attr['floatvalue'] = $val;
									}else{
										$attr['value'] = $val;
									}
									$part['Spec'][] = $attr;
								}
							}
						}else{
							$attr = array();
							$attr['part_attribute_id'] = $attribute['PartAttribute']['id'];
							//$attr['name'] = 'value';
							if($attribute['PartAttribute']['type']=='number'){
								$attr['floatvalue'] = $value;
							}else{
								$attr['value'] = $value;
							}
							$part['Spec'][] = $attr;
						}
					}
				}
			}
		}
		
		$this->Part->create();
		return $this->Part->saveAll($part, array('deep'=>true));
	}
}
