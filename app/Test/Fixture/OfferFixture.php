<?php
/**
 * OfferFixture
 *
 */
class OfferFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'avail' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'buynow_url' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 256, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'clickthrough_url' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 256, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'is_authorized' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 4),
		'is_brokered' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 4),
		'packaging' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 64, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'sendrfq_url' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 256, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'sku' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 256, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'brand_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'fk_offers_brands1' => array('column' => 'brand_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'avail' => 1,
			'buynow_url' => 'Lorem ipsum dolor sit amet',
			'clickthrough_url' => 'Lorem ipsum dolor sit amet',
			'is_authorized' => 1,
			'is_brokered' => 1,
			'packaging' => 'Lorem ipsum dolor sit amet',
			'sendrfq_url' => 'Lorem ipsum dolor sit amet',
			'sku' => 'Lorem ipsum dolor sit amet',
			'brand_id' => 1
		),
	);
}
