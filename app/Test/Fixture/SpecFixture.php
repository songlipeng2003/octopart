<?php
/**
 * SpecFixture
 *
 */
class SpecFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'part_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
		'part_attribute_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
		'value' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 64, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'fk_parts_has_part_attributes_part_attributes1' => array('column' => 'part_attribute_id', 'unique' => 0), 'fk_parts_has_part_attributes_parts1' => array('column' => 'part_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'part_id' => 1,
			'part_attribute_id' => 1,
			'value' => 'Lorem ipsum dolor sit amet'
		),
	);
}
