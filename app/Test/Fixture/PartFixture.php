<?php
/**
 * PartFixture
 *
 */
class PartFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'mpn' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 64, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'brand_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
		'detail_url' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 64, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'avg_price' => array('type' => 'float', 'null' => true, 'default' => NULL),
		'avg_avail' => array('type' => 'float', 'null' => true, 'default' => NULL),
		'market_status' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 64, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'num_suppliers' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'num_authsuppliers' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'short_description' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 256, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'fk_parts_brands1' => array('column' => 'brand_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'mpn' => 'Lorem ipsum dolor sit amet',
			'brand_id' => 1,
			'detail_url' => 'Lorem ipsum dolor sit amet',
			'avg_price' => 1,
			'avg_avail' => 1,
			'market_status' => 'Lorem ipsum dolor sit amet',
			'num_suppliers' => 1,
			'num_authsuppliers' => 1,
			'short_description' => 'Lorem ipsum dolor sit amet'
		),
	);
}
