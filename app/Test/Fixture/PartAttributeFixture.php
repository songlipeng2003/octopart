<?php
/**
 * PartAttributeFixture
 *
 */
class PartAttributeFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'fieldname' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 64, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'displayname' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 64, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'type' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 4),
		'datatype' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 4),
		'unit_name' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 64, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'unit_ symbol' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 64, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'fieldname' => 'Lorem ipsum dolor sit amet',
			'displayname' => 'Lorem ipsum dolor sit amet',
			'type' => 1,
			'datatype' => 1,
			'unit_name' => 'Lorem ipsum dolor sit amet',
			'unit_ symbol' => 'Lorem ipsum dolor sit amet'
		),
	);
}
