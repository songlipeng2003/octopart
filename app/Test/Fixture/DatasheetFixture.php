<?php
/**
 * DatasheetFixture
 *
 */
class DatasheetFixture extends CakeTestFixture {
/**
 * Table name
 *
 * @var string
 */
	public $table = 'datasheet';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'score' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'url' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 256, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'part_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'fk_datasheet_parts1' => array('column' => 'part_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'score' => 1,
			'url' => 'Lorem ipsum dolor sit amet',
			'part_id' => 1
		),
	);
}
