<?php
/**
 * PriceFixture
 *
 */
class PriceFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'offer_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
		'part_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
		'number' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'price' => array('type' => 'float', 'null' => true, 'default' => NULL),
		'currency' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 64, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'fk_prices_offers1' => array('column' => 'offer_id', 'unique' => 0), 'fk_prices_parts1' => array('column' => 'part_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'offer_id' => 1,
			'part_id' => 1,
			'number' => 1,
			'price' => 1,
			'currency' => 'Lorem ipsum dolor sit amet'
		),
	);
}
