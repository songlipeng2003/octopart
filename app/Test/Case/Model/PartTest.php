<?php
App::uses('Part', 'Model');

/**
 * Part Test Case
 *
 */
class PartTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.part', 'app.brand', 'app.image', 'app.datasheet', 'app.description', 'app.part_category', 'app.price', 'app.offer');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Part = ClassRegistry::init('Part');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Part);

		parent::tearDown();
	}

}
