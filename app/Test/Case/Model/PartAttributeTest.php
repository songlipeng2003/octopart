<?php
App::uses('PartAttribute', 'Model');

/**
 * PartAttribute Test Case
 *
 */
class PartAttributeTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.part_attribute');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PartAttribute = ClassRegistry::init('PartAttribute');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PartAttribute);

		parent::tearDown();
	}

}
