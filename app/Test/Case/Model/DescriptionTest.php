<?php
App::uses('Description', 'Model');

/**
 * Description Test Case
 *
 */
class DescriptionTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.description', 'app.part');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Description = ClassRegistry::init('Description');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Description);

		parent::tearDown();
	}

}
