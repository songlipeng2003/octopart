<?php
App::uses('Datasheet', 'Model');

/**
 * Datasheet Test Case
 *
 */
class DatasheetTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.datasheet', 'app.part');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Datasheet = ClassRegistry::init('Datasheet');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Datasheet);

		parent::tearDown();
	}

}
