<?php 
/**
 * 通过调用 php curl 库函数 取远程 $url 中的内容
 */
function web_fetch($url, $array=null){
	extract($array);
	
	static $wait_times = 1;
	
	$referer=substr($url,0,strpos($url,'/',7));

	$host=substr($referer,7);
	//$referer = $referer . '/api/v2/parts/search';
	$referer = $referer . '/partbrowser';

	$m_curl = curl_init();
	$header[] = "Host: $host";
	$header[] = "text/html,application/xhtml+xml,text/javascript,application/javascript,q=0.9,*/*;q=0.8";
	$header[] = "Cache-Control: max-age=0";
	$header[] = "Connection: keep-alive";
	$header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
	$header[] = "Accept-Language: en-us,en;q=0.5";
	//$header[] = "X-Requested-With: XMLHttpRequest";
	
	if(isset($cookie)){
		if($cookie===true){
			$cookie_file = TMP.'cookies.tmp';
			curl_setopt($m_curl, CURLOPT_COOKIEFILE, $cookie_file);
		}else{
			$cookies = '';
			$count = count($cookie);
			$i = 0;
			foreach($cookie as $key=>$value){
				$i++;
				$cookies.=$key.'='.$value;
				if($i!=$count){
					$cookies.='; ';
				}
			}
			
			curl_setopt ($m_curl, CURLOPT_COOKIE , $cookies);
		}
	}

	curl_setopt($m_curl, CURLOPT_URL, $url);
	curl_setopt($m_curl, CURLOPT_HTTPHEADER, $header);
	curl_setopt($m_curl, CURLOPT_REFERER, $referer);
	curl_setopt($m_curl, CURLOPT_ENCODING, 'gzip,deflate');
	curl_setopt($m_curl, CURLOPT_AUTOREFERER, true);
	curl_setopt($m_curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($m_curl, CURLOPT_CONNECTTIMEOUT, 15);
	curl_setopt($m_curl, CURLOPT_TIMEOUT, 60);
	
	$agent = isset($agent)?$agent:'google';
	if($agent=='google'){
		curl_setopt($m_curl, CURLOPT_USERAGENT, 'Googlebot/2.1 (+http://www.google.com/bot.html)');
	}elseif($agent=='firefox'){
		curl_setopt($m_curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3');
	}
	
	$proxy = isset($proxy)?$proxy:false;
	if(is_array($proxy)){
		if(!empty($proxy['host']) && !empty($proxy['port'])) {
			curl_setopt($m_curl, CURLOPT_PROXY, $proxy['host'].':'.$proxy['port']);
		}
		if(!empty($proxy['type']) && $proxy['type']=='socks5') {
			curl_setopt($m_curl, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
		}
	}

	$html = curl_exec($m_curl);
	
	if(strpos($html, '403 Forbidden') > 0 || strpos($html, 'uncovered a bug in our code') > 0 ){
		$html = '';
		$wait_times = $wait_times*2;
	}else{
		curl_close($m_curl);	
		return $html;
	}
	
	$is_retry = isset($is_retry)?$is_retry:false;
	$max_try_times = isset($try_times)?$try_times:3;
	$try_times=0;
	while(!$html && $is_retry && $try_times<$max_try_times){
		// echo "等待".$wait_times."秒\n";
		sleep($wait_times);
		$html = curl_exec($m_curl);
		if(strpos($html, '403 Forbidden') > 0 || strpos($html, 'uncovered a bug in our code') > 0 ){
			$html = '';
		}
		
		if($html==''){
			$wait_times = $wait_times*2;
			$try_times++;
		}
	}

	curl_close($m_curl);

	$wait_times = 1;
	//sleep($wait_times);
	
	return $html;
}
