<?php
const SESSION_FILTER_KEY = 'SESSION_FILTER_KEY';
 
/**
 * octopart 的过滤器
 * 
 * @author zhouyong
 *
 */
class FormFilter{
	var $categoryId = 0;
	var $q_attributeid = '0'; 
	var $q_manufacturers = '0';
	var $q_suppliers ='0';
	var $q_keyword = '';
	/**
	 * 
	 * 储存属性过滤条件
	 * @var array
	 */
	var $attribs = array();
}



/**
 * php  整数集合交集优化 
 * int_array_intersect操作的都是整数 更快
 * @var array
 */
function int_array_intersect()
{
    if (func_num_args() < 2) {
        trigger_error('param error', E_USER_ERROR);
    }
 
    $args = func_get_args();
 
    foreach ($args AS $arg) {
        if (!is_array($arg)) {
            trigger_error('param error', E_USER_ERROR);
        }
    }
 
    $intersect = function($a, $b) {
        $result = array();
 
        $length_a = count($a);
        $length_b = count($b);
 
        for ($i = 0, $j = 0; $i < $length_a && $j < $length_b; null) {
            if($a[$i] < $b[$j] && ++$i) {
                continue;
            }
 
            if($a[$i] > $b[$j] && ++$j) {
                continue;
            }
 
            $result[] = $a[$i];
 
            if (isset($a[$next = $i + 1]) && $a[$next] != $a[$i]) {
                ++$j;
            }
            ++$i;
        }
 
        return $result;
    };
 
    $result = array_shift($args);
    sort($result);
 
    foreach ($args as $arg) {
        sort($arg);
        $result = $intersect($result, $arg);
    }
 
    return $result;
}