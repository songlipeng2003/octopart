<?php
App::uses('AppModel', 'Model');
/**
 * Spec Model
 *
 * @property Part $Part
 * @property PartAttribute $PartAttribute
 */
class Spec extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'value';

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Part' => array(
			'className' => 'Part',
			'foreignKey' => 'part_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'PartAttribute' => array(
			'className' => 'PartAttribute',
			'foreignKey' => 'part_attribute_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
