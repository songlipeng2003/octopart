<?php
App::uses('AppModel', 'Model');
/**
 * Datasheet Model
 *
 * @property Part $Part
 */
class Datasheet extends AppModel {
/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'datasheet';
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'url';

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Part' => array(
			'className' => 'Part',
			'foreignKey' => 'part_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
