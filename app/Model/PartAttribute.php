<?php
App::uses('AppModel', 'Model');
/**
 * PartAttribute Model
 *
 */
class PartAttribute extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'displayname';
}
