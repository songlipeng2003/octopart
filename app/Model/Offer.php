<?php
App::uses('AppModel', 'Model');
/**
 * Offer Model
 *
 * @property Brand $Brand
 * @property Price $Price
 */
class Offer extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'sku';

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Brand' => array(
			'className' => 'Brand',
			'foreignKey' => 'brand_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Price' => array(
			'className' => 'Price',
			'foreignKey' => 'offer_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
