<?php
App::uses('AppModel', 'Model');
/**
 * PartContent Model
 *
 */
class PartContent extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'url';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
}
