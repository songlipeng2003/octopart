<?php
App::uses('AppModel', 'Model');
/**
 * Description Model
 *
 * @property Part $Part
 */
class Description extends AppModel {
/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'description';
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'text';

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Part' => array(
			'className' => 'Part',
			'foreignKey' => 'part_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
