<?php
App::uses('AppModel', 'Model');
/**
 * Price Model
 *
 * @property Offer $Offer
 * @property Part $Part
 */
class Price extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'price';

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Offer' => array(
			'className' => 'Offer',
			'foreignKey' => 'offer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
}
