

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE */;
/*!40101 SET SQL_MODE='' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES */;
/*!40103 SET SQL_NOTES='ON' */;



USE `octopart`;

#
# Dumping data for table part_attributes
#

REPLACE INTO `part_attributes` VALUES (1,'size_height','Size-Height','number','float','m','m','尺寸-高度');
REPLACE INTO `part_attributes` VALUES (2,'termination_style','Termination Style','text',NULL,NULL,NULL,'终端类型');
REPLACE INTO `part_attributes` VALUES (3,'number_of_pins','Number of Pins','number','integer',NULL,NULL,'引脚数');
REPLACE INTO `part_attributes` VALUES (4,'capacitance','Capacitance','number','float','F','F','电容');
REPLACE INTO `part_attributes` VALUES (5,'size_width','Size-Width','number','float','m','m','尺寸-宽度');
REPLACE INTO `part_attributes` VALUES (6,'voltage_rating_dc','Voltage Rating (DC)','number','float','V','V','额定电压（DC）');
REPLACE INTO `part_attributes` VALUES (7,'lead_free_status','Lead-Free Status','text',NULL,NULL,NULL,'无铅状态');
REPLACE INTO `part_attributes` VALUES (8,'dielectric_material','Dielectric Material','text',NULL,NULL,NULL,'绝缘材料');
REPLACE INTO `part_attributes` VALUES (9,'packaging','Packaging','text',NULL,NULL,NULL,'包装');
REPLACE INTO `part_attributes` VALUES (10,'size_length','Size-Length','number','float','m','m','尺寸-长度');
REPLACE INTO `part_attributes` VALUES (11,'mounting_type','Mounting Type','text',NULL,NULL,NULL,'Mounting Type');
REPLACE INTO `part_attributes` VALUES (12,'case_package','Case / Package / Footprint','text',NULL,NULL,NULL,'包装');
REPLACE INTO `part_attributes` VALUES (13,'rohs','RoHS','text',NULL,NULL,NULL,'符合RoHS');
REPLACE INTO `part_attributes` VALUES (14,'insulation_resistance','Insulation Resistance','number','float','ohms','Ω','绝缘电阻');
REPLACE INTO `part_attributes` VALUES (15,'size_thickness','Size-Thickness','number','float','m','m','尺寸厚度');
REPLACE INTO `part_attributes` VALUES (16,'temperature_coefficient','Temperature Coefficient','number','float','ppm/degC','ppm/°C','温度系数');
REPLACE INTO `part_attributes` VALUES (17,'equivalent_series_resistance_esr','Equivalent Series Resistance (ESR)','number','float','ohms','Ω','等效串联电阻（ESR）');
REPLACE INTO `part_attributes` VALUES (18,'size_diameter','Size-Diameter','number','float','m','m','尺寸直径');
REPLACE INTO `part_attributes` VALUES (19,'voltage_rating_ac','Voltage Rating (AC)','number','float','V','V','额定电压（AC）');
REPLACE INTO `part_attributes` VALUES (20,'lead_length','Lead Length','number','float','m','m','导线长度');
REPLACE INTO `part_attributes` VALUES (21,'current_rating','Current Rating','number','float','A','A','额定电流');
REPLACE INTO `part_attributes` VALUES (22,'color','Color','text',NULL,NULL,NULL,'颜色');
REPLACE INTO `part_attributes` VALUES (23,'q_factor','Q-Factor','number','float',NULL,NULL,'Q-Factor');
REPLACE INTO `part_attributes` VALUES (24,'material','Material','text',NULL,NULL,NULL,'材料');
REPLACE INTO `part_attributes` VALUES (25,'size_inner_diameter','Size-Inner Diameter','number','float','m','m','尺寸内径');
REPLACE INTO `part_attributes` VALUES (26,'power_rating','Power Rating','number','float','W','W','额定功率');
REPLACE INTO `part_attributes` VALUES (27,'resistance','Resistance','number','float','ohms','Ω','电阻');
REPLACE INTO `part_attributes` VALUES (28,'resistance_tolerance','Resistance Tolerance','number','float','%','%','电阻宽容差');
REPLACE INTO `part_attributes` VALUES (29,'number_of_contacts','Number of Contacts','number','integer',NULL,NULL,'触点数');
REPLACE INTO `part_attributes` VALUES (30,'holding_current','Holding Current','number','float','A','A','保持电流');
REPLACE INTO `part_attributes` VALUES (31,'tripping_current','Tripping Current','number','float','A','A','脱扣电流');
REPLACE INTO `part_attributes` VALUES (32,'flammability_rating','Flammability Rating','text',NULL,NULL,NULL,'可燃性等级');
REPLACE INTO `part_attributes` VALUES (33,'operating_voltage','Operating Voltage','number','float','V','V','工作电压');
REPLACE INTO `part_attributes` VALUES (34,'time_to_trip','Time to Trip','number','float','s','s','跳闸时间');
REPLACE INTO `part_attributes` VALUES (35,'part_family','Part Family','text',NULL,NULL,NULL,'零件族');
REPLACE INTO `part_attributes` VALUES (36,'power_dissipation','Power Dissipation','number','float','W','W','功耗');
REPLACE INTO `part_attributes` VALUES (37,'gbw','Gain Bandwidth Product','number','float','Hz','Hz','增益带宽积');
REPLACE INTO `part_attributes` VALUES (38,'supply_voltage_dc','Supply Voltage (DC)','number','float','V','V','供电电压（DC）');
REPLACE INTO `part_attributes` VALUES (39,'slew_rate','Slew Rate','number','float','V/microsecond','V/μs','摆率');
REPLACE INTO `part_attributes` VALUES (40,'input_bias_current','Input Bias Current','number','float','A','A','输入偏置电流');
REPLACE INTO `part_attributes` VALUES (41,'number_of_channels','Number of Channels','number','integer',NULL,NULL,'通道数');
REPLACE INTO `part_attributes` VALUES (42,'number_of_circuits','Number of Circuits','number','integer',NULL,NULL,'电路数');
REPLACE INTO `part_attributes` VALUES (43,'lifecycle_status','Lifecycle Status','text',NULL,NULL,NULL,'生命周期状态');
REPLACE INTO `part_attributes` VALUES (44,'self_resonant_frequency','Self Resonant Frequency','number','float','Hz','Hz','自谐振频率');
REPLACE INTO `part_attributes` VALUES (45,'input_voltage_dc','Input Voltage (DC)','number','float','V','V','输入电压（DC）');
REPLACE INTO `part_attributes` VALUES (46,'quiescent_current','Quiescent Current','number','float','A','A','静态电流');
REPLACE INTO `part_attributes` VALUES (47,'output_voltage','Output Voltage','number','float','V','V','输出电压');
REPLACE INTO `part_attributes` VALUES (48,'output_current','Output Current','number','float','A','A','输出电流');
REPLACE INTO `part_attributes` VALUES (49,'supply_current','Supply Current','number','float','A','A','供电电流');
REPLACE INTO `part_attributes` VALUES (50,'weight','Weight','number','float','g','g','重量');
REPLACE INTO `part_attributes` VALUES (51,'resonant_frequency','Resonant Frequency','number','float','Hz','Hz','共振频率');
REPLACE INTO `part_attributes` VALUES (52,'logic_type','Digital Logic Level','text',NULL,NULL,NULL,'数字逻辑电平');
REPLACE INTO `part_attributes` VALUES (53,'capacitance_tolerance','Capacitance Tolerance','number','float','%','%','电容公差');
REPLACE INTO `part_attributes` VALUES (54,'insulation_material','Insulation Material','text',NULL,NULL,NULL,'绝缘材料');
REPLACE INTO `part_attributes` VALUES (55,'data_rate','Data Rate','number','float','bps','bps','数据传输速率');
REPLACE INTO `part_attributes` VALUES (56,'voltage_nodes','Voltage Nodes','number','float','V','V','电压节点');
REPLACE INTO `part_attributes` VALUES (57,'number_of_outputs','Number of Outputs','number','integer',NULL,NULL,'输出数');
REPLACE INTO `part_attributes` VALUES (58,'number_of_gates','Number of Gates','number','integer',NULL,NULL,'盖茨数');
REPLACE INTO `part_attributes` VALUES (59,'wire_gauge','Wire Gauge','number','float','wiregauge','AWG','线规');
REPLACE INTO `part_attributes` VALUES (60,'polarity','Polarity','text',NULL,NULL,NULL,'极性');
REPLACE INTO `part_attributes` VALUES (61,'forward_voltage','Forward Voltage','number','float','V','V','正向电压');
REPLACE INTO `part_attributes` VALUES (62,'pin_pitch','Pin Pitch','number','float','m','m','引脚间距');
REPLACE INTO `part_attributes` VALUES (63,'orientation','Orientation','text',NULL,NULL,NULL,'取向');
REPLACE INTO `part_attributes` VALUES (64,'actuator_type','Actuator Type','text',NULL,NULL,NULL,'Actuator Type');
REPLACE INTO `part_attributes` VALUES (65,'housing_color','Housing Color','text',NULL,NULL,NULL,'外壳颜色');
REPLACE INTO `part_attributes` VALUES (66,'number_of_positions','Number of Positions','number','integer',NULL,NULL,'位置数');
REPLACE INTO `part_attributes` VALUES (67,'contact_material','Contact Material','text',NULL,NULL,NULL,'触点材料');
REPLACE INTO `part_attributes` VALUES (68,'contact_plating','Contact Plating','text',NULL,NULL,NULL,'触点电镀');
REPLACE INTO `part_attributes` VALUES (69,'frequency','Frequency','number','float','Hz','Hz','频率');
REPLACE INTO `part_attributes` VALUES (70,'inductance','Inductance','number','float','H','H','电感');
REPLACE INTO `part_attributes` VALUES (71,'contact_current_rating','Contact Current Rating','number','float','A','A','触点额定电流');
REPLACE INTO `part_attributes` VALUES (72,'contacts_type','Contacts Type','text',NULL,NULL,NULL,'触点类型');
REPLACE INTO `part_attributes` VALUES (73,'contact_resistance','Contact Resistance','number','float','ohms','Ω','接触电阻');
REPLACE INTO `part_attributes` VALUES (74,'lens_color','Lens Color','text',NULL,NULL,NULL,'镜片颜色');
REPLACE INTO `part_attributes` VALUES (75,'luminous_intensity','Luminous Intensity','number','float','cd','cd','发光强度');
REPLACE INTO `part_attributes` VALUES (76,'housing_material','Housing Material','text',NULL,NULL,NULL,'外壳材质');
REPLACE INTO `part_attributes` VALUES (77,'coil_resistance','Coil Resistance','number','float','ohms','Ω','线圈电阻');
REPLACE INTO `part_attributes` VALUES (78,'number_of_rows','Number of Rows','number','integer',NULL,NULL,'');
REPLACE INTO `part_attributes` VALUES (79,'operating_temperature','Operating Temperature','number','float','degC','°C','工作温度');
REPLACE INTO `part_attributes` VALUES (80,'input_current','Input Current','number','float','A','A','输入电流');
REPLACE INTO `part_attributes` VALUES (81,'coil_voltage','Coil Voltage','number','float','V','V','线圈电压');
REPLACE INTO `part_attributes` VALUES (82,'carry_current','Carry Current','number','float','A','A','通电电流');
REPLACE INTO `part_attributes` VALUES (83,'rise_time','Rise Time','number','float','s','s','上升时间');
REPLACE INTO `part_attributes` VALUES (84,'switching_frequency','Switching Frequency','number','float','Hz','Hz','开关频率');
REPLACE INTO `part_attributes` VALUES (85,'cable_length','Cable Length','number','float','m','m','电缆长度');
REPLACE INTO `part_attributes` VALUES (86,'gender','Gender','text',NULL,NULL,NULL,'');
REPLACE INTO `part_attributes` VALUES (87,'mounting_angle','Mounting Angle','number','float','degree','?°','安装角');
REPLACE INTO `part_attributes` VALUES (88,'power_consumption','Power Consumption','number','float','W','W','功耗');
REPLACE INTO `part_attributes` VALUES (89,'peak_wavelength','Peak Wavelength','number','float','m','m','峰值波长');
REPLACE INTO `part_attributes` VALUES (90,'breakdown_voltage','Breakdown Voltage','number','float','V','V','击穿电压');
REPLACE INTO `part_attributes` VALUES (91,'wavelength','Wavelength','number','float','m','m','波长');
REPLACE INTO `part_attributes` VALUES (92,'number_of_detents','Number of Detents','number','integer',NULL,NULL,'');
REPLACE INTO `part_attributes` VALUES (93,'shielding','Shielding','text',NULL,NULL,NULL,'屏蔽');
REPLACE INTO `part_attributes` VALUES (94,'dropout_voltage','Dropout Voltage','number','float','V','V','电压差');
REPLACE INTO `part_attributes` VALUES (95,'character_size_height','Character Size-Height','number','float','m','m','');
REPLACE INTO `part_attributes` VALUES (96,'switching_current','Switching Current','number','float','A','A','开关电流');
REPLACE INTO `part_attributes` VALUES (97,'isolation_voltage','Isolation Voltage','number','float','V','V','隔离电压');
REPLACE INTO `part_attributes` VALUES (98,'output_power','Output Power','number','float','W','W','输出功率');
REPLACE INTO `part_attributes` VALUES (99,'rds_drain_to_source_resistance_on','Drain to Source Resistance (on) (Rds)','number','float','ohms','Ω','漏极与源电阻（RDS）');
REPLACE INTO `part_attributes` VALUES (100,'id_continuous_drain_current','Continuous Drain Current (Ids)','number','float','A','A','连续漏电流（IDS）');
REPLACE INTO `part_attributes` VALUES (101,'vds_drain_to_source_voltage','Drain to Source Voltage (Vds)','number','float','V','V','漏极至源极电压（VDS）');
REPLACE INTO `part_attributes` VALUES (102,'memory_size','Memory Size','number','integer','byte','B','内存大小');
REPLACE INTO `part_attributes` VALUES (103,'sample_rate','Sample Rate','number','float','Hz','Hz','采样率');
REPLACE INTO `part_attributes` VALUES (104,'lens_type','Lens Type','text',NULL,NULL,NULL,'镜头类型');
REPLACE INTO `part_attributes` VALUES (105,'input_capacitance','Input Capacitance','number','float','F','F','输入电容');
REPLACE INTO `part_attributes` VALUES (106,'clock_speed','Clock Speed','number','float','Hz','Hz','时钟速度');
REPLACE INTO `part_attributes` VALUES (107,'number_of_i_o_pins','Number of I/O Pins','number','integer',NULL,NULL,'I / O引脚数');
REPLACE INTO `part_attributes` VALUES (108,'number_of_adcs','Number of ADCs','number','integer',NULL,NULL,'ADC数');
REPLACE INTO `part_attributes` VALUES (109,'output_current_drive','Output Current Drive','number','float','A','A','输出电流驱动');
REPLACE INTO `part_attributes` VALUES (110,'cord_length','Cord Length','number','float','m','m','Cord的长度');
REPLACE INTO `part_attributes` VALUES (111,'load_capacitance','Load Capacitance','number','float','F','F','负载电容');
REPLACE INTO `part_attributes` VALUES (112,'number_of_conductors','Number of Conductors','number','integer',NULL,NULL,'导体数量');
REPLACE INTO `part_attributes` VALUES (113,'conductor_material','Conductor Material','text',NULL,NULL,NULL,'导体材料');
REPLACE INTO `part_attributes` VALUES (114,'jacket_material','Jacket Material','text',NULL,NULL,NULL,'护套材料');
REPLACE INTO `part_attributes` VALUES (115,'number_of_mating_cycles','Number of Mating Cycles','number','integer',NULL,NULL,'Mating周期数');
REPLACE INTO `part_attributes` VALUES (116,'shrouded','Shrouded','text',NULL,NULL,NULL,'');
REPLACE INTO `part_attributes` VALUES (117,'number_of_outlets','Number of Outlets','number','integer',NULL,NULL,'网点数量');
REPLACE INTO `part_attributes` VALUES (118,'mated_height','Mated Height','number','float','m','m','插配高度');
REPLACE INTO `part_attributes` VALUES (119,'halogen_free_status','Halogen Free Status','text',NULL,NULL,NULL,'无卤状态');
REPLACE INTO `part_attributes` VALUES (120,'glow_wire_compliant','Glow Wire Compliant','text',NULL,NULL,NULL,'灼热丝标准');
REPLACE INTO `part_attributes` VALUES (121,'drivers_per_package','Drivers Per Package','number','integer',NULL,NULL,'每包装的驱动Driver');
REPLACE INTO `part_attributes` VALUES (122,'available_channels','Available Channels','text',NULL,NULL,NULL,'');
REPLACE INTO `part_attributes` VALUES (123,'number_of_regulated_outputs','Number of Regulated Outputs','number','integer',NULL,NULL,'稳压输出数量');
REPLACE INTO `part_attributes` VALUES (124,'topology','Topology','text',NULL,NULL,NULL,'拓扑');
REPLACE INTO `part_attributes` VALUES (125,'thermal_shutdown','Thermal Shutdown','text',NULL,NULL,NULL,'');
REPLACE INTO `part_attributes` VALUES (126,'offset_drift','Offset Drift','number','float','V/K','V/K','零点漂移');
REPLACE INTO `part_attributes` VALUES (127,'threshold_voltage','Threshold Voltage','number','float','V','V','阈值电压');
REPLACE INTO `part_attributes` VALUES (128,'output_capacitor_type','Output Capacitor Type','text',NULL,NULL,NULL,'输出电容器型');
REPLACE INTO `part_attributes` VALUES (129,'frequency_tolerance','Frequency Tolerance','number','float','ppm','ppm','频率Tolerance');
REPLACE INTO `part_attributes` VALUES (130,'oscillator_type','Oscillator Type','text',NULL,NULL,NULL,'振荡器类型');
REPLACE INTO `part_attributes` VALUES (131,'rise_fall_time','Rise/Fall Time','number','float','s','s','上升/下降时间');
REPLACE INTO `part_attributes` VALUES (132,'frequency_stability','Frequency Stability','number','float','ppm','ppm','频率稳定性');
REPLACE INTO `part_attributes` VALUES (133,'ram_bytes','RAM Memory Size','number','integer','byte','B','RAM内存大小');
REPLACE INTO `part_attributes` VALUES (134,'access_time','Access Time','number','float','s','s','访问时间');
REPLACE INTO `part_attributes` VALUES (135,'flash_memory_bytes','FLASH Memory Size','number','integer','byte','B','闪存FLASH大小');
REPLACE INTO `part_attributes` VALUES (136,'number_of_uart','Number of Serial Ports (UART)','number','integer',NULL,NULL,'串口（UART）数量');
REPLACE INTO `part_attributes` VALUES (137,'processor_type','Processor Type','text',NULL,NULL,NULL,'处理器类型');
REPLACE INTO `part_attributes` VALUES (138,'breakdown_voltage_drain_to_source','Breakdown Voltage (Drain to Source)','number','float','V','V','击穿电压（漏极到源）');
REPLACE INTO `part_attributes` VALUES (139,'breakdown_voltage_gate_to_source','Breakdown Voltage [Gate to Source]','number','float','V','V','击穿电压栅源');
REPLACE INTO `part_attributes` VALUES (140,'gate_charge','Gate Charge','number','float','C','C',NULL);
REPLACE INTO `part_attributes` VALUES (141,'gain','Gain','number','float','dB','dB','增益');
REPLACE INTO `part_attributes` VALUES (142,'breakdown_voltage_collector_to_emitter','Breakdown Voltage [Collector to Emitter]','number','float','V','V','击穿电压集电极到发射极');
REPLACE INTO `part_attributes` VALUES (143,'breakdown_voltage_collector_to_base','Breakdown Voltage [Collector to Base]','number','float','V','V','击穿电压集电极基');
REPLACE INTO `part_attributes` VALUES (144,'switching_voltage','Switching Voltage','number','float','V','V','开关电压');
REPLACE INTO `part_attributes` VALUES (145,'input_power','Input Power','number','float','W','W','输入功率');
REPLACE INTO `part_attributes` VALUES (146,'static_current','Static Current','number','float','A','A','静态电流');

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
