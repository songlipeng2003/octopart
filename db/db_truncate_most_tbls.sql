
use `octopart`;

--  当需要重新从 part_contents 构建数据库的数据时，此文件用来清空 旧的数据表 

SET FOREIGN_KEY_CHECKS=0;
UPDATE `octopart`.`part_contents` SET is_process = 0 ,   is_fetch=1;

TRUNCATE `octopart`.`brands`;
TRUNCATE `octopart`.`datasheet`;
TRUNCATE `octopart`.`description`;
TRUNCATE `octopart`.`hyperlinks`;
TRUNCATE `octopart`.`images`;
TRUNCATE `octopart`.`offers`;
TRUNCATE `octopart`.`part_category`;
TRUNCATE `octopart`.`parts`;
TRUNCATE `octopart`.`prices`;
TRUNCATE `octopart`.`specs`;

SET FOREIGN_KEY_CHECKS=1;
