
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE */;
/*!40101 SET SQL_MODE='' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES */;
/*!40103 SET SQL_NOTES='ON' */;



DROP DATABASE IF EXISTS `octopart`;
CREATE DATABASE `octopart` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `octopart`;

#
# Source for table brands
#

DROP TABLE IF EXISTS `brands`;
CREATE TABLE `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `displayname` varchar(64) DEFAULT NULL,
  `homepage_url` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

#
# Source for table categories
#

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `nodename` varchar(256) DEFAULT NULL,
  `image` varchar(256) DEFAULT NULL,
  `image_40px` varchar(256) DEFAULT NULL,
  `image_50px` varchar(256) DEFAULT NULL,
  `children_ids` varchar(256) DEFAULT NULL,
  `ancestor_ids` varchar(256) DEFAULT NULL,
  `num_parts` int(11) DEFAULT NULL,
  `title_cn` varchar(255) DEFAULT '' COMMENT 'title by chinese',
  PRIMARY KEY (`id`),
  KEY `fk_categories_categories` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

#
# Source for table datasheet
#

DROP TABLE IF EXISTS `datasheet`;
CREATE TABLE `datasheet` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `score` int(11) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `part_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_datasheet_parts1` (`part_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

#
# Source for table description
#

DROP TABLE IF EXISTS `description`;
CREATE TABLE `description` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `credit_domain` varchar(64) DEFAULT NULL,
  `credit_url` varchar(256) DEFAULT NULL,
  `text` text,
  `part_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_description_parts1` (`part_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

#
# Source for table hyperlinks
#

DROP TABLE IF EXISTS `hyperlinks`;
CREATE TABLE `hyperlinks` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `freesample` varchar(256) DEFAULT NULL,
  `evalkit` varchar(256) DEFAULT NULL,
  `manufacturer` varchar(256) DEFAULT NULL,
  `part_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hyperlinks_parts1` (`part_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Source for table images
#

DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `part_id` bigint(20) DEFAULT NULL,
  `credit_domain` varchar(64) DEFAULT NULL,
  `credit_url` varchar(256) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `url_30px` varchar(256) DEFAULT NULL,
  `url_35px` varchar(256) DEFAULT NULL,
  `url_55px` varchar(256) DEFAULT NULL,
  `url_90px` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_images_parts1` (`part_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

#
# Source for table offers
#

DROP TABLE IF EXISTS `offers`;
CREATE TABLE `offers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `avail` int(11) DEFAULT NULL,
  `buynow_url` varchar(256) DEFAULT NULL,
  `clickthrough_url` varchar(256) DEFAULT NULL,
  `is_authorized` tinyint(4) DEFAULT NULL,
  `is_brokered` tinyint(4) DEFAULT NULL,
  `packaging` varchar(64) DEFAULT NULL,
  `sendrfq_url` varchar(256) DEFAULT NULL,
  `sku` varchar(256) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `part_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_offers_brands1` (`brand_id`),
  KEY `fk_offers_parts1` (`part_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

#
# Source for table part_attributes
#

DROP TABLE IF EXISTS `part_attributes`;
CREATE TABLE `part_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldname` varchar(64) DEFAULT NULL,
  `displayname` varchar(64) DEFAULT NULL,
  `type` varchar(64) DEFAULT NULL,
  `datatype` varchar(64) DEFAULT NULL,
  `unit_name` varchar(64) DEFAULT NULL,
  `unit_symbol` varchar(64) DEFAULT NULL,
  `title_cn` varchar(255) DEFAULT '' COMMENT 'title by chinese',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

#
# Source for table part_category
#
 
DROP TABLE IF EXISTS `part_category`;
CREATE TABLE `part_category` (
  `part_id` bigint(20) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`part_id`,`category_id`),
  KEY `fk_parts_has_categories_categories1` (`category_id`),
  KEY `fk_parts_has_categories_parts1` (`part_id`),
  KEY `ix_category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



  
#
# Source for table part_contents
#

DROP TABLE IF EXISTS `part_contents`;
CREATE TABLE `part_contents` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(256) DEFAULT NULL,
  `content` longtext,
  `is_fetch` tinyint(4) DEFAULT NULL,
  `is_process` tinyint(4) DEFAULT NULL,
  `number` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

#
# Source for table parts
#

DROP TABLE IF EXISTS `parts`;
CREATE TABLE `parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) DEFAULT NULL,
  `mpn` varchar(256) DEFAULT NULL,
  `detail_url` varchar(256) DEFAULT NULL,
  `avg_price` double DEFAULT NULL,
  `avg_avail` double DEFAULT NULL,
  `avg_price_unit` varchar(64) DEFAULT NULL,
  `market_status` varchar(64) DEFAULT NULL,
  `num_suppliers` int(11) DEFAULT NULL,
  `num_authsuppliers` int(11) DEFAULT NULL,
  `short_description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_parts_brands1` (`brand_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

#
# Source for table prices
#

DROP TABLE IF EXISTS `prices`;
CREATE TABLE `prices` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `offer_id` bigint(20) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `currency` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_prices_offers1` (`offer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

#
# Source for table specs
#

DROP TABLE IF EXISTS `specs`;
CREATE TABLE `specs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `part_id` bigint(20) DEFAULT NULL,
  `part_attribute_id` int(11) DEFAULT NULL,
  `value` varchar(64) DEFAULT NULL,
  `floatvalue` double DEFAULT '0' COMMENT 'float or int value attribute',
  PRIMARY KEY (`id`),
  KEY `fk_parts_has_part_attributes_part_attributes1` (`part_attribute_id`),
  KEY `fk_parts_has_part_attributes_parts1` (`part_id`),
  KEY `ix_attr_id` (`part_attribute_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

#
#  Foreign keys for table categories
#

ALTER TABLE `categories`
ADD CONSTRAINT `fk_categories_categories` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

#
#  Foreign keys for table datasheet
#

ALTER TABLE `datasheet`
ADD CONSTRAINT `fk_datasheet_parts1` FOREIGN KEY (`part_id`) REFERENCES `parts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

#
#  Foreign keys for table description
#

ALTER TABLE `description`
ADD CONSTRAINT `fk_description_parts1` FOREIGN KEY (`part_id`) REFERENCES `parts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

#
#  Foreign keys for table hyperlinks
#

ALTER TABLE `hyperlinks`
ADD CONSTRAINT `fk_hyperlinks_parts1` FOREIGN KEY (`part_id`) REFERENCES `parts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

#
#  Foreign keys for table images
#

ALTER TABLE `images`
ADD CONSTRAINT `fk_images_parts1` FOREIGN KEY (`part_id`) REFERENCES `parts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

#
#  Foreign keys for table offers
#

ALTER TABLE `offers`
ADD CONSTRAINT `fk_offers_brands1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_offers_parts1` FOREIGN KEY (`part_id`) REFERENCES `parts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

#
#  Foreign keys for table part_category
#

ALTER TABLE `part_category`
ADD CONSTRAINT `fk_parts_has_categories_categories1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_parts_has_categories_parts1` FOREIGN KEY (`part_id`) REFERENCES `parts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

#
#  Foreign keys for table parts
#

ALTER TABLE `parts`
ADD CONSTRAINT `fk_parts_brands1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

#
#  Foreign keys for table prices
#

ALTER TABLE `prices`
ADD CONSTRAINT `fk_prices_offers1` FOREIGN KEY (`offer_id`) REFERENCES `offers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

#
#  Foreign keys for table specs
#

ALTER TABLE `specs`
ADD CONSTRAINT `fk_parts_has_part_attributes_parts1` FOREIGN KEY (`part_id`) REFERENCES `parts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_parts_has_part_attributes_part_attributes1` FOREIGN KEY (`part_attribute_id`) REFERENCES `part_attributes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;


/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
